<?php
$bdd = new PDO('mysql:host=localhost;dbname=tableaumktn;charset=utf8', 'root', '');
session_start();

if ($_SESSION['id_connexion']==null) {
  header("Location: ../accueil/connexion.php");
}
?>

<html>
	<head>
		<meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css"/>
        <link rel="stylesheet" href="../css/menu.css"/>
        <link href="../www/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    	    <title>Formulaire</title>
    </head>
<?php
$reqPM = 'SELECT * FROM pm';
   $pm = $bdd->prepare($reqPM);
   $pm->execute();
   $donneesPM = $pm->fetchAll(PDO::FETCH_ASSOC);

if (isset($_POST['bouton'])) {
	$numPA=$_POST["numPA"];
	$_SESSION["numPA"]=$numPA;

	$idPM = $_POST["idPM"];
	$_SESSION["idPM"]=$idPM;

	$capacite=$_POST["capacite"];
	$_SESSION["capacite"]= $capacite;

	$nomCableEntree=$_POST["numerotr"];
	$_SESSION["nomCableEntree"]=$nomCableEntree;

	$nbCable=$_POST["nbcablesortie"];//info a prendre en champs
	$_SESSION["nbCable"]=$nbCable;
}
?> 
  <body class="index">
     <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>
     <?php include ("../menu/menu.php") ?>
  <form action="#" method="POST">
    <fieldset>
          <legend style = "color : #156094">Création de la boite</legend>

          <table>
        <tr>
        	<th><label for="numPA">Numéro PA : </label></th>
        	<td><input  name="numPA" style="width: 200px;"<?php if(isset($_POST['bouton'])||isset($_POST['bouton2'])){?> placeholder="<?php echo $_SESSION["numPA"]; ?>"<?php }?> /></td>
        </tr>
        <tr><td>&nbsp</td></tr>
        <tr>
        	<th><label for="idPM">PM : </label></th>
        	<div class="form-group"> 
                <td><SELECT value="idPM" name="idPM" type="text" size="1">
                  <?php
                  foreach ($donneesPM AS $donneePM)
                  {
                    ?>
                  <option <?php if ( isset($_POST['bouton'])||isset($_POST['bouton2']) AND $_SESSION["idPM"]==$donneePM['idPM']) { echo "selected"; } ?> value="<?php echo $donneePM['idPM']; ?>"> <?php echo $donneePM['numPM']."- ".$donneePM['adresse']; ?> </option>
                  <?php
                  }
                  ?>
              </SELECT></td>
          	  </div>
		
        </tr>
        <tr><td>&nbsp</td></tr>
        <tr>
        	<th><label for="capacite">Capacité : </label></th>
        	<td><SELECT value="capacite" name="capacite" size="1" style="width: 200px;">
         	<OPTION <?php if (isset($_POST['bouton'])||isset($_POST['bouton2']) AND $_SESSION["capacite"]==6) { 
         		echo "selected"; } ?>>6</OPTION>
         	<OPTION <?php if (isset($_POST['bouton'])||isset($_POST['bouton2']) AND $_SESSION["capacite"]==12) { 
         		echo "selected"; } ?>>12</OPTION>
         	<OPTION <?php if (isset($_POST['bouton'])||isset($_POST['bouton2']) AND $_SESSION["capacite"]==24) { 
         		echo "selected"; } ?>>24</OPTION>
         	<OPTION <?php if (isset($_POST['bouton'])||isset($_POST['bouton2']) AND $_SESSION["capacite"]==36) { 
         		echo "selected"; } ?>>36</OPTION>
         	<OPTION <?php if (isset($_POST['bouton'])||isset($_POST['bouton2']) AND $_SESSION["capacite"]==48) { 
         		echo "selected"; } ?>>48</OPTION>
         	<OPTION <?php if (isset($_POST['bouton'])||isset($_POST['bouton2']) AND $_SESSION["capacite"]==72) { 
         		echo "selected"; } ?>>72</OPTION>
         	<OPTION <?php if (isset($_POST['bouton'])||isset($_POST['bouton2']) AND $_SESSION["capacite"]==96) { 
         		echo "selected"; } ?>>96</OPTION>
         	<OPTION <?php if (isset($_POST['bouton'])||isset($_POST['bouton2']) AND $_SESSION["capacite"]==144) { 
         		echo "selected"; } ?>>144</OPTION>
         		</SELECT>
         	</td>
        </tr>
        <tr><td>&nbsp</td></tr>
        <tr>
        	<th><label for="numerotr"> Numéro du TR entrée : </label></th>
        	<td><input type="text" name="numerotr" style="width: 200px;" <?php if(isset($_POST['bouton'])||isset($_POST['bouton2'])){?> placeholder="<?php echo $_SESSION["nomCableEntree"]; ?>"<?php }?> /><br></td>
        </tr>
        <tr><td>&nbsp</td></tr>
        <tr>
        	<th><label for="nbcablesortie">Nombres de cables sortie : </label></th>
        	<td><input type="text" name="nbcablesortie" style="width: 200px;"<?php if(isset($_POST['bouton'])||isset($_POST['bouton2'])){?> placeholder="<?php echo $_SESSION["nbCable"]; ?>"<?php }?> /><br></td>
        </tr>
        <tr><td>&nbsp</td></tr>
        <tr>
          <th></th>
          <td><button type="submit" name="bouton" class="btn btn-primary"><b>Suivant</b></button></td>
        </tr>
           </table> 
      </fieldset>

</form>
<?php
//session_start();
//$nbtube=$capacite/6; // les T avec pour chaque tube 6 fibres
//$nbcassette=$capacite/12; // 12 cassettes repartis en fonction de la capacite de Fo total
$fibre=1;
$fibreSortie=1;

$numCable=0;

//$capaciteCableSortie[0]=24;
//$capaciteCableSortie[1]=6;
//$capaciteCableSortie[2]=48;
//boucle pour remplir le tableau avec les capacité de chaque cables
	
if (isset($_POST["bouton"])){
	
?>
 <form method="POST">
 	<?php
	for ($j=1; $j<$_SESSION["nbCable"]+1; $j++){

	?>
			<b>Renseignements TR sortie</b> <br>
	<table>
		<tr>
			<th><label for="numerotr"> Numéro du câble:</label></th>
    		<td><input type="text" name="numerotr<?php echo $j ?>"/><br></td>
    	</tr>
        <tr><td>&nbsp</td></tr>
    	<tr>
    		<th><label for="numerotr"> Numéro du PB: </label></th>
    		<td><input type="text" name="numeropb<?php echo $j ?>"/><br></td>
    	</tr>
        <tr><td>&nbsp</td></tr>
    	<tr>
   			<th> <label for="capacite">Capacité : </label> </th>
   			<td> <SELECT value="capacite<?php echo $j ?>" name="capacite<?php echo $j ?>" size="1">
   			 	<OPTION>6</OPTION>
   			 	<OPTION>12</OPTION>
   			 	<OPTION>24</OPTION>
   			 	<OPTION>36</OPTION>
   			 	<OPTION>48</OPTION>
   			 	<OPTION>72</OPTION>
   			 	<OPTION>96</OPTION>
   			 	<OPTION>144</OPTION>
   			 </SELECT>
   			<br></td>
   		</tr>
        <tr><td>&nbsp</td></tr>
   		<tr>
   			<th><label for="nbTube"> Nombre de tubes utiles : </label></th>
        	<td><input type="text" name="nbTube<?php echo $j ?>"/><br></td>
        </tr>
          <tr><td>&nbsp</td></tr>
        <tr>
   			<th><label for="numerotr"> TR du câble: </label></th>
    		<td><input type="text" name="trcable<?php echo $j ?>"/><br></td>
    	</tr>
      <tr><td>&nbsp</td></tr>
      <tr><td>&nbsp</td></tr>
    	<hr width="50%">
    	</table>

<?php
	}
?>
  <table>
    <tr>
      <th>&nbsp</th>
      <td><button type="submit" name="boutonPB" class="btn btn-primary"><b>Renseigner PB</b></button><br><br><br><br></td>
    </tr>
  </table>

	
 </form>		
 

    <?php 
}if (isset($_POST["boutonPB"])) { 

    	for ($j=1; $j<$_SESSION["nbCable"]+1; $j++){

    		$numeroCableSortie[$j-1]=$_POST["numerotr".$j];
    		$_SESSION["numeroCableSortie"][$j-1] = $numeroCableSortie[$j-1];
	
   			$numPB[$j-1] = $_POST["numeropb".$j];
   			$_SESSION["numPB"][$j-1] = $numPB[$j-1];
	
			$capaciteCableSortie[$j-1]=$_POST["capacite".$j];
			$_SESSION["capaciteCableSortie"][$j-1] = $capaciteCableSortie[$j-1];
	
			$nbTube[$j-1]=$_POST["nbTube".$j]*6;
			$_SESSION["nbTube"][$j-1] = $nbTube[$j-1];
	
			$TRcable[$j-1] =$_POST["trcable".$j];
			$_SESSION["trcable"][$j-1] = $TRcable[$j-1];
	?>

  <form method="POST">
    		<table class="table">
    			<thead>
    				<tr>

    					<th>TR <?php echo $numeroCableSortie[$j-1]; ?></th>

    				</tr>
            <tr><td>&nbsp</td></tr>
    			</thead>
    			<tbody>
    				<tr>
    				<?php
    					for ($i=0; $i<$nbTube[$j-1]/6; $i++){
    						$h= $j.$i;
					?>
    					<td>
    						<table class="table">
    						<tbody>
    							<tr>
    								<th><label for="pb<?php echo $i ?>"> PB <?php echo $i+1 ?>: </label></th>
    								<td><input type="text" name="pb<?php echo $h ?>"/></td><br>
    							</tr>
                  <tr><td>&nbsp</td></tr>
    							<tr>
    								<th><label for="pt<?php echo $i ?>"> PT <?php echo $i+1 ?>: </label></th>
    								<td><input type="text" name="pt<?php echo $h ?>"/><br></td>
    							</tr>
                  <tr><td>&nbsp</td></tr>
    						</tbody>
    						</table>
    					</td>
    				<?php } ?>
    				</tr>
    			</tbody>
    		</table>
    	<?php
    	}
    	?>
    <button type="submit" name="bouton2" class="btn btn-primary"><b>Créer</b></button><br><br><br><br>
</form>

<?php
}
  if(isset($_POST["bouton2"])){

	$fibre=1;
	$fibreSortie=1;
	$pbTab = 1;

	$numCable=0;

	$req = $bdd->prepare('INSERT INTO pa(numPA, idPM, capaentree,TRentree, NBcablesortie) VALUES(:numPA, :idPM, :capaentree, :TRentree, :NBcablesortie)');
        
    $req->execute(array(
    ':numPA' => $_SESSION["numPA"],
    ':idPM' => $_SESSION["idPM"],
    ':capaentree' => $_SESSION["capacite"],
    ':TRentree' => $_SESSION["nomCableEntree"],
    ':NBcablesortie' => $_SESSION["nbCable"]
     ));

    $idPA = $bdd->lastInsertId();
    $_SESSION["idPA"]=$idPA;

   	for ($j=1; $j<$_SESSION["nbCable"]+1; $j++){

		$req = $bdd->prepare('INSERT INTO cablesortie(numCable, idPA, numPB, capaCable, nbTube, TRcable) VALUES(:numCable, :idPA, :numPB, :capaCable, :nbTube, :TRcable)');
        
    	$req->execute(array(
    	':numCable' => $_SESSION["numeroCableSortie"][$j-1],
    	':idPA' => $_SESSION["idPA"],
    	':numPB' => $_SESSION["numPB"][$j-1],
    	':capaCable' => $_SESSION["capaciteCableSortie"][$j-1],
    	':nbTube' => $_SESSION["nbTube"][$j-1],
    	':TRcable' => $_SESSION["trcable"][$j-1]
    	 ));
    	$idCable = $bdd->lastInsertId();

  		for ($i=0; $i<$_SESSION["nbTube"][$j-1]/6; $i++){
  			$h = $j.$i;
  		
  			$pb[$h-1] = $_POST["pb".$h];
  			$pt[$h-1] = $_POST["pt".$h];

  			$req = $bdd->prepare('INSERT INTO pb(idCable, TRpb, PT) VALUES(:idCable, :TRpb, :PT)');
        
    		$req->execute(array(
    		':idCable' => $idCable,
    		':TRpb' => $pb[$h-1],
    		':PT' => $pt[$h-1]
    		 ));
  		}
  	}
}
?>
<div class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>
</body>
    <script src="../www/bootstrap/js/jquery.js"></script>
    <script src="../www/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../www/js/bootstrap.min.js"></script>

</html>