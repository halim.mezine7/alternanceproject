<?php
$bdd = new PDO('mysql:host=localhost;dbname=tableaumktn;charset=utf8', 'root', '');
session_start();
if ($_SESSION['id_connexion']==null) {
  header("Location: ../accueil/connexion.php");
}

 $reqPA = 'SELECT * FROM pa';
 $pa = $bdd->prepare($reqPA);
 $pa->execute();
 $donneesPA = $pa->fetchAll(PDO::FETCH_ASSOC);
 $fibre=1;
 $fibreSortie=1;
 $numCable=0;
 $nPB = 0;
 


?>
 <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link href="../www/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    </head>
    <body class="index">
     <div class="banniere">
        <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>
  <?php if (!isset($_POST["bouton"])) {
include("../menu/menu.php"); ?>

<form action="#" method="POST">
    <fieldset>
    	<table>
          <legend style = "color : #156094">Choisir PA</legend>
        
            <div class="form-group">
            <tr> 
            	<th><label for="numPA">PA : </label></th>
                <td><SELECT value="idPA" name="idPA" type="text" size="1" style="width: 200px;">
                  
                  <?php
                  foreach ($donneesPA AS $donneePA)
                  {
                    ?>
                <option value="<?php echo $donneePA['idPA'];?>">
                  <?php echo $donneePA['numPA']?></option>

                  <?php
                  }
                  ?>
              </SELECT></td>
            </tr>
            </div>
            <tr><td>&nbsp</td></tr>
   			<tr>
   				<th></th>
            	<td><button type="submit" name="bouton" class="btn btn-primary"><b>Choisir</b></button></td>
            </tr>
        </table>
    </fieldset>
  </form>

<?php 
	
 }if (isset($_POST["bouton"])) {

	$idPA = $_POST["idPA"];
	$_SESSION["idPA"] = $idPA;
   	
   	$req = $bdd -> prepare ('SELECT * FROM pa, pm
                            WHERE pm.idPM = pa.idPM
                            AND pa.idPA = ?');
  	$req ->execute(array($idPA));
  	$info = $req -> fetch(PDO::FETCH_ASSOC);


	$numPA=$info["numPA"];
	$_SESSION["numPA"]=$numPA;

	$idPM = $info["idPM"];
	$_SESSION["idPM"]=$idPM;

	$capacite= $info["capaentree"];
	$_SESSION["capacite"]= $capacite;

	$nomCableEntree= $info["TRentree"];
	$_SESSION["nomCableEntree"]=$nomCableEntree;

	$nbCable= $info["NBcablesortie"];
	$_SESSION["nbCable"]=$nbCable;
	//echo $nbCable;


	$cable = $bdd->query("SELECT * FROM cablesortie WHERE idPA = ".$idPA."");
	$totalTube = 0;
	$j=1;
    while ($donnees = $cable -> fetch()){
    	$idCable[$j-1] = $donnees["idCable"];
    	$numeroCableSortie[$j-1] = $donnees["TRcable"];
    	$numPB[$j-1] = $donnees["numPB"];
    	$capaciteCableSortie[$j-1] = $donnees["capaCable"];
    	$nbTube[$j-1]=$donnees["nbTube"];
    	$TRcable[$j-1] = $donnees["TRcable"];
    	$j++;
    }
    $_SESSION["totalTube"]=$totalTube;
    //$pbTab = 1;
    ?>
    <?php include("../menu/menu.php"); ?>
<form action="#" method="POST">
    <fieldset>
    	<table>
          <legend style = "color : #156094">Choisir PA</legend>
        

            <div class="form-group">
            <tr> 
            	<th><label for="numPA">PA : </label></th>
                <td><SELECT value="idPA" name="idPA" type="text" size="1" style="width:175px;">
                  
                  <?php
                  foreach ($donneesPA AS $donneePA)
                  {
                    ?>
                <option value="<?php echo $donneePA['idPA'];?>"><?php echo $donneePA['numPA']?></option>

                  <?php
                  }
                  ?>
              </SELECT></td>
            </tr>
            </div>
            <tr><td>&nbsp</td></tr>
   			<tr>
   				<th></th>
            	<td><button type="submit" name="bouton" class="btn btn-primary"><b>Choisir</b></button></td>
            </tr>
        </table>
    </fieldset>
  </form>
<?php
	echo "<table border='1'>";
	for($i=1;$i<=$_SESSION["capacite"];$i++)
	{
		$tubeactuel=0;
		echo "<tr>";
		//colonne entre cable
		if ($i==1)
		{
			echo "<td rowspan=".$capacite.">".$capacite."FO <br> TR: ".$nomCableEntree."</td>";
		}
		if ($fibre==7)
		{
			$fibre=1;
		}
		//colonne tube entree
		switch ($i)
		{
			case 1:
				echo "<td rowspan='6' BGCOLOR='red'>T1</td>";
				break;
			case 7:
				echo "<td rowspan='6' BGCOLOR='blue'>T2</td>";
				break;
			case 13:
				echo "<td rowspan='6' BGCOLOR='green'>T3</td>";
				break;
			case 19:
				echo "<td rowspan='6' BGCOLOR='yellow'>T4</td>";
				break;
			case 25:
				echo "<td rowspan='6' BGCOLOR='purple'>T5</td>";
				break;
			case 31:
				echo "<td rowspan='6' BGCOLOR='white'>T6</td>";
				break;
			case 37:
				echo "<td rowspan='6' BGCOLOR='orange'>T7</td>";
				break;
			case 43:
				echo "<td rowspan='6' BGCOLOR='grey'>T8</td>";
				break;
			case 49:
				echo "<td rowspan='6' BGCOLOR='brown'>T9</td>";
				break;
			case 55:
				echo "<td rowspan='6' COLOR='blue' BGCOLOR='black'>T10</td>";
				break;
			case 61:
				echo "<td rowspan='6' BGCOLOR='turquoise'>T11</td>";
				break;
			case 67:
				echo "<td rowspan='6' BGCOLOR='pink'>T12</td>";
				break;
			case 73:
				echo "<td rowspan='6' BGCOLOR='red'>T13</td>";
				break;
			case 79:
				echo "<td rowspan='6' BGCOLOR='blue'>T14</td>";
				break;
			case 85:
				echo "<td rowspan='6' BGCOLOR='green'>T15</td>";
				break;
			case 91:
				echo "<td rowspan='6' BGCOLOR='yellow'>T16</td>";
				break;
			case 97:
				echo "<td rowspan='6' BGCOLOR='purple'>T17</td>";
				break;
			case 103:
				echo "<td rowspan='6' BGCOLOR='white'>T18</td>";
				break;
			case 109:
				echo "<td rowspan='6' BGCOLOR='orange'>T19</td>";
				break;
			case 115:
				echo "<td rowspan='6' BGCOLOR='grey'>T20</td>";
				break;
			case 121:
				echo "<td rowspan='6' BGCOLOR='brown'>T21</td>";
				break;
			case 127:
				echo "<td rowspan='6' BGCOLOR='black'>T22</td>";
				break;
			case 133:
				echo "<td rowspan='6' BGCOLOR='blue'>T23</td>";
				break;
			case 139:
				echo "<td rowspan='6' BGCOLOR='pink'>T24</td>";
				break;
			//endswitch;
		}
		//colonne fibre
		switch ($fibre)
		{
			case 1:
				echo "<td BGCOLOR='red'>F1</td>";
				break;
			case 2:
				echo "<td BGCOLOR='blue'>F2</td>";
				break;
			case 3:
				echo "<td BGCOLOR='green'>F3</td>";
				break;
			case 4:
				echo "<td BGCOLOR='yellow'>F4</td>";
				break;
			case 5:
				echo "<td BGCOLOR='purple'>F5</td>";
				break;
			case 6:
				echo "<td BGCOLOR='white'>F6</td>";
				break;
			
			//endswitch;
		}
		//colonne casette
		switch ($i)
		{
			case 1:
				echo "<td rowspan='12'>cassette 1</td>";
				break;
			case 13:
				echo "<td rowspan='12'>cassette 2</td>";
				break;
			case 25:
				echo "<td rowspan='12'>cassette 3</td>";
				break;
			case 37:
				echo "<td rowspan='12'>cassette 4</td>";
				break;
			case 49:
				echo "<td rowspan='12'>cassette 5</td>";
				break;
			case 61:
				echo "<td rowspan='12'>cassette 6</td>";
				break;
			case 73:
				echo "<td rowspan='12'>cassette 7</td>";
				break;
			case 85:
				echo "<td rowspan='12'>cassette 8</td>";
				break;
			case 97:
				echo "<td rowspan='12'>cassette 9</td>";
				break;
			case 109:
				echo "<td rowspan='12'>cassette 10</td>";
				break;
			case 121:
				echo "<td rowspan='12'>cassette 11</td>";
				break;
			case 133:
				echo "<td rowspan='12'>cassette 12</td>";
				break;
			//endswitch;
		}
		//colonne fibre sortie
		switch ($fibre)
		{
			case 1:
				echo "<td BGCOLOR='red'>F1</td>";
				break;
			case 2:
				echo "<td BGCOLOR='blue'>F2</td>";
				break;
			case 3:
				echo "<td BGCOLOR='green'>F3</td>";
				break;
			case 4:
				echo "<td BGCOLOR='yellow'>F4</td>";
				break;
			case 5:
				echo "<td BGCOLOR='purple'>F5</td>";
				break;
			case 6:
				echo "<td BGCOLOR='white'>F6</td>";
				break;
			//endswitch;
		}
		//colonne tube sortie
		switch ($fibreSortie)
		{
			case 1:
				echo "<td rowspan='6' BGCOLOR='red'>T1</td>";
				$tubeactuel=1;
				break;
			case 7:
				echo "<td rowspan='6' BGCOLOR='blue'>T2</td>";
				$tubeactuel=1;
				break;
			case 13:
				echo "<td rowspan='6' BGCOLOR='green'>T3</td>";
				$tubeactuel=1;
				break;
			case 19:
				echo "<td rowspan='6' BGCOLOR='yellow'>T4</td>";
				$tubeactuel=1;
				break;
			case 25:
				echo "<td rowspan='6' BGCOLOR='purple'>T5</td>";
				$tubeactuel=1;
				break;
			case 31:
				echo "<td rowspan='6' BGCOLOR='white'>T6</td>";
				$tubeactuel=1;
				break;
			case 37:
				echo "<td rowspan='6' BGCOLOR='orange'>T7</td>";
				$tubeactuel=1;
				break;
			case 43:
				echo "<td rowspan='6' BGCOLOR='grey'>T8</td>";
				$tubeactuel=1;
				break;
			case 49:
				echo "<td rowspan='6' BGCOLOR='brown'>T9</td>";
				$tubeactuel=1;
				break;
			case 55:
				echo "<td rowspan='6' COLOR='white' BGCOLOR='black'>T10</td>";
				$tubeactuel=1;
				break;
			case 61:
				echo "<td rowspan='6' BGCOLOR='blue'>T11</td>";
				$tubeactuel=1;
				break;
			case 67:
				echo "<td rowspan='6' BGCOLOR='pink'>T12</td>";
				$tubeactuel=1;
				break;
			case 73:
				echo "<td rowspan='6' BGCOLOR='red'>T13</td>";
				$tubeactuel=1;
				break;
			case 79:
				echo "<td rowspan='6' BGCOLOR='blue'>T14</td>";
				$tubeactuel=1;
				break;
			case 85:
				echo "<td rowspan='6' BGCOLOR='green'>T15</td>";
				$tubeactuel=1;
				break;
			case 91:
				echo "<td rowspan='6' BGCOLOR='yellow'>T16</td>";
				$tubeactuel=1;
				break;
			case 97:
				echo "<td rowspan='6' BGCOLOR='purple'>T17</td>";
				$tubeactuel=1;
				break;
			case 103:
				echo "<td rowspan='6' BGCOLOR='white'>T18</td>";
				$tubeactuel=1;
				break;
			case 109:
				echo "<td rowspan='6' BGCOLOR='orange'>T19</td>";
				$tubeactuel=1;
				break;
			case 115:
				echo "<td rowspan='6' BGCOLOR='grey'>T20</td>";
				$tubeactuel=1;
				break;
			case 121:
				echo "<td rowspan='6' BGCOLOR='brown'>T21</td>";
				$tubeactuel=1;
				break;
			case 127:
				echo "<td rowspan='6' BGCOLOR='black'>T22</td>";
				$tubeactuel=1;
				break;
			case 133:
				echo "<td rowspan='6' BGCOLOR='blue'>T23</td>";
				$tubeactuel=1;
				break;
			case 139:
				echo "<td rowspan='6' BGCOLOR='pink'>T24</td>";
				$tubeactuel=1;
				break;
			//endswitch;
		}
		
		if ($fibreSortie==1)
		{
			$numCableAffiche=$numCable+1;
			echo "<td rowspan=".$nbTube[$numCable].">Cable ".$numCableAffiche."<br> TR: ".$numeroCableSortie[$numCable]."<br> PB: ".$numPB[$numCable]."<br>".$capaciteCableSortie[$numCable]."Fo</td>";
		}
		$id = $idCable[$numCable];
		/*while($array)
		{
			$tr[$p]=$array['TRpb'];
			$p++;
		}*/
		//echo '<SCRIPT language="Javascript">alert(\''.$nPB.'\');</SCRIPT>';
		// echo '<SCRIPT language="Javascript">alert(\''.$nb.'\');</SCRIPT>';		
		if($tubeactuel==1)
		{
			$cable = $bdd->prepare("SELECT TRpb, PT FROM pb WHERE idCable = ".$id."");
			$cable ->execute();
			$array = $cable -> fetchAll(PDO::FETCH_ASSOC);
			$nb = count($array);
			for ($p=0; $p<$nb; $p++)
			{
				//echo $array[$p]['TRpb'];
				$tr[$p]=$array[$p]['TRpb'];
				$pt[$p]=$array[$p]['PT'];
			}
			//echo "<td rowspan='6'>".$cable[$nPB]."</td>";
			echo "<td rowspan='6'>PB : ".$tr[$nPB]."<br> PT :".$pt[$nPB]."</td>";

			if ($nPB < $nb-1) 
			{
				$nPB++;
			}
			else 
			{
				$nPB = 0;
			}
		}
		
		if ($fibreSortie==$nbTube[$numCable])
		{
			if($numCable<$nbCable-1)
			{
				$fibreSortie=1;
				$numCable++;
			}
		}
		else
		{
			$fibreSortie++;
		}
		$fibre++;
		
		echo "</tr>";
	}
echo "</table>";
}
?>
<br><br><br><br>
<div class="footer">
	<br>
  MKTN GROUP-2018 ©
  	<br><br>
</div>
</body>
 	<script src="../www/bootstrap/js/jquery.js"></script>
    <script src="../www/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../www/js/bootstrap.min.js"></script>

</html>