<?php 
session_start();
  $connect = mysqli_connect("localhost", "root", "", "tableaumktn");

  if ($_SESSION['id_connexion']==null) {
  header("Location: ../accueil/connexion.php");
}
  $query = "SELECT * FROM touret, projet, client, stockage WHERE touret.idProjet = projet.idProjet AND client.idClient = touret.idClient AND stockage.idStockage = touret.idStockage AND Rendu = 1 ORDER BY idTouret";
  $result = mysqli_query($connect, $query);

  include('../PHPExcel/FichierExcel.php');
$fichier = new FichierExcel();

if(isset($_POST['export'])){
    $info = $_SESSION['donnees'];
    $fichier->Insertion($info);
    $fichier->output('Stock Rendu');
  }
?>

<head>
  <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link rel="stylesheet" type="text/css" href="../js/datatables.css">
        <link rel="stylesheet" type="text/css" href="../js/DataTables-1.10.16/css/dataTables.bootstrap.min.js">
        <link href="../www/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
        <script type="text/javascript" charset="utf8" src="../js/datatables.js">
        </script>
        <script type="text/javascript" charset="utf8" src="../js/DataTables-1.10.16/js/dataTables.bootstrap.min.js">
        </script>
        <script type="text/javascript" charset="utf8" src="../js/DataTables-1.10.16/js/jquery.dataTables.min.js">
        </script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
        <title>Affichage des stocks</title>
</head>

<body class="index">
     <div class="banniere">
       <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>
  <?php include ("../menu/menu.php");
  $donnees = "Nom ; Cable ; Reference ; Client ; Projet ; Stockage ; Longueur restantes ; Longueur Totale ; Date rendu"."\n";
  ?>

<center><h2> Affichage des stocks rendu</h2></center>
<div class="container">
<div class="container">
<table class="display" id="example">

  <thead> 
    <tr class="cable">
    <th>Nom</th>
    <th>Cable</th>
    <th>Reference</th>
    <th>Client</th>
    <th>Projet</th>
    <th>Stockage</th>
    <th>Longueur restantes</th>
    <th>Longueur Totale</th>
    <th>Date rendu</th>
  </tr>
  </thead>
  
  <tbody>
  <?php 
    while($row = mysqli_fetch_array($result)){
  ?> 
    <tr class="gradeX"> 
      <td><?php echo $row['nomTouret']; $donnees = $donnees . $row['nomTouret'].";"; ?></td>
      <td><?php echo $row['typeCable']; $donnees = $donnees . $row['typeCable'].";"; ?></td>
      <td><?php echo $row['reference']; $donnees = $donnees . $row['reference'].";"; ?></td>
      <td><?php echo $row['nomClient']; $donnees = $donnees . $row['nomClient'].";"; ?></td>
      <td><?php echo $row['typeProjet']; $donnees = $donnees . $row['typeProjet'].";"; ?></td>
      <td><?php echo $row['typeStockage']; $donnees = $donnees . $row['typeStockage'].";"; ?></td>
      <td><?php echo $row['longueur']; $donnees = $donnees . $row['longueur'].";"; ?></td>
      <td><?php echo $row['longueurTotale']; $donnees = $donnees . $row['longueurTotale'].";"; ?></td>
      <td><?php echo $row['dateRendu']; $donnees = $donnees . $row['dateRendu']."\n"; ?></td>  
    </tr> 
  <?php } ?>
  </tbody>

</table>
<form action="" method="POST">
    <button type="submit" name="export" class="btn btn-primary"><b>Exporter excel</b></button>
</form>
<script type="text/javascript">
$(document).ready(function () {

    $('#example').DataTable({

        language: {

            url: "../js/French.json"

        }

    });

});
</script>
<div border="solid" class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>
<?php $_SESSION['donnees'] = $donnees; ?>
</body>
</div>