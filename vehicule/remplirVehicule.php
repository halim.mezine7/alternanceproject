<?php 
$bdd = new PDO('mysql:host=localhost;dbname=tableaumktn;charset=utf8', 'root', ''); 
session_start();

ini_set( 'display_errors', 'on' );
error_reporting( E_ALL );
// requete qui permet de selectionner les attributs de la table touret
$req ="SELECT * FROM vehicule ";
    $vehicule = $bdd->prepare($req);
    $vehicule->execute();
    $donneesVehicule = $vehicule->fetchAll(PDO::FETCH_ASSOC);

if (isset($_POST['boutonchoisir'])) {
      $idVehicule = $_POST['idVehicule'];
      $_SESSION['idVehicule'] = $idVehicule;
      header('Location: remplirVehicule2.php');
    }
?>
<html>
<head>
    <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link href="../www/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  <title>Formulaire</title>
</head>
<body class="index">
     <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>

<?php include ("../menu/menu.php");
?>

  <form action="#" method="POST">
     <div class="form-group">
    <fieldset>
      <table>
        <legend style = "color : #156094">Modification d'un véhicule</legend>
       
        <tr>
          <th><br><label for="idVehicule">Choisir Modéle du véhicule : </label></th>
          <td><br><SELECT name="idVehicule" value ="idVehicule" size="1" style="width: 200px; ">
            <?php
                  foreach ($donneesVehicule AS $donneeVehicule)
                  {
                    ?>
                  
                  <option value="<?php echo $donneeVehicule['idVehicule'];?>">
                  <?php echo $donneeVehicule['modele']; ?></option>
                  <?php
                  }
                  ?>
          </SELECT></td>
        </tr>

      <tr>
          <th></th>
          <td><br><button type="submit" name="boutonchoisir" class="btn btn-primary"><b>Choisir</b></button></td>
        </tr>
        
       </table>   
      </fieldset>
        </div>
  </form>

<div class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>
</body>
    <script src="../www/bootstrap/js/jquery.js"></script>
    <script src="../www/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../www/js/bootstrap.min.js"></script>
</html>
