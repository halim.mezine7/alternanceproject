<?php 
$bdd = new PDO('mysql:host=localhost;dbname=tableaumktn;charset=utf8', 'root', ''); 
session_start();



$req = 'SELECT * FROM vehicule';
   $vehicule = $bdd->prepare($req);
   $vehicule->execute();
   $donneesVehicule = $vehicule->fetchAll(PDO::FETCH_ASSOC);


  $req5 = 'SELECT * FROM etat';
      $etat = $bdd->prepare($req5);
      $etat->execute();
      $donneesEtat = $etat->fetchAll(PDO::FETCH_ASSOC);

  $reqv = 'SELECT * FROM vehiculeprobleme';
      $vehiculeprobleme = $bdd->prepare($reqv);
      $vehiculeprobleme->execute();
      $donneesProbleme = $vehiculeprobleme->fetchAll(PDO::FETCH_ASSOC);
  
?>

<html>
<head>
  <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link rel="stylesheet" type="text/css" href="../js/datatables.css">
        <link rel="stylesheet" type="text/css" href="../js/DataTables-1.10.16/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
        <script type="text/javascript" charset="utf8" src="../js/datatables.js">
        </script>
        <script type="text/javascript" charset="utf8" src="../js/DataTables-1.10.16/js/dataTables.bootstrap.min.js">
        </script>
        <script type="text/javascript" charset="utf8" src="../js/DataTables-1.10.16/js/jquery.dataTables.min.js">
        </script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <script src="../js/jQuery.msgBox-master/scripts/jquery.msgBox.js" type="text/javascript"></script>
    <link href="../js/jQuery.msgBox-master/styles/msgBoxLight.css" rel="stylesheet" type="text/css">


   <title>Formulaire</title>
  </head>
<body class="index">
     <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>
     <?php
 include ("../menu/menu.php"); 

 if (!isset($_POST["boutonrique"])){
?>
 <form action="#" method="POST">
      <table>
          <tr>
            <h2>Choisir l'historique d'un véhicule</h2>
          </tr>
        <tr>
          <th><br><label for="idVehicule">Choisir Vehicule :  </label></th>
          <td><br><SELECT name="idVehicule" value ="idVehicule" size="1" style="width: 200px; ">
            <?php
                  foreach ($donneesVehicule AS $donneeVehicule)
                  {
                    ?>
                  
                  <option value="<?php echo $donneeVehicule['idVehicule'];?>"<?php if (isset($_SESSION["idVehicule"])) {
                    if ($_SESSION["idVehicule"]==$donneeVehicule['idVehicule']) {echo "selected";} }?>>
                  <?php echo $donneeVehicule['modele']; ?></option>
                  <?php
                  }
                  ?>
          </SELECT></td>
        </tr>
 <tr>
          <th></th>
          <td><br><button type="submit" name="boutonrique" class="btn btn-primary"><b>Ajouter</b></button></td>
        </tr>
</table>
</form>
<?php  
}
 if (isset ($_POST["boutonrique"])){
      $idVehicule = $_POST["idVehicule"];
      $_SESSION["idVehicule"]=$idVehicule;


?>
 <form action="#" method="POST">
 <table>
          <tr>
            <h2>Choisir l'historique d'un véhicule</h2>
          </tr>
          <div class="form-group">
        <tr>
          <th><br><label for="idVehicule">Choisir Vehicule :  </label></th>
          <td><br><SELECT name="idVehicule" value ="idVehicule" type ="text" size="1" style="width: 200px;">
            <?php
                  foreach ($donneesVehicule AS $donneeVehicule)
                  {
                    ?>
                  <option value="<?php echo $donneeVehicule['idVehicule'];?>"<?php if (isset($_SESSION["idVehicule"])) {
                    if ($_SESSION["idVehicule"]==$donneeVehicule['idVehicule']) {echo "selected";} }?>>
                  <?php echo $donneeVehicule['modele']; ?></option>
                  <?php
                  }
                  ?>
          </SELECT></td>
        </tr>
      </div>
 <tr>
          <th></th>
          <td><br><button type="submit" name="boutonrique" class="btn btn-primary"><b>Choisir</b></button></td>
        </tr>
</table>




<h2>Historique du Chauffeur</h2>
<table class="table table-striped">
<thead>
  <tr class="chauffeur">
    <th>Modèle</th>
    <th>Nom du Chauffeur</th>
    <th>Date changement de chauffeur</th>
    <th></th>
  </tr>
</thead>   

  
  <?php
$req4 = $bdd->query("SELECT * FROM vehicule, historiqueChauffeur, chauffeur WHERE vehicule.idVehicule = ".$idVehicule." AND vehicule.idVehicule = historiquechauffeur.idVehicule AND chauffeur.idChauffeur = historiqueChauffeur.idChauffeur ORDER BY dateChauffeur");


while ($historiquechauffeur = $req4->fetch()) { 

?>
<tbody>
<tr>
  <td><?php echo $historiquechauffeur['modele']; ?></td>
  <td><?php echo $historiquechauffeur['nomChauffeur']; ?></td>
  <td><?php echo $historiquechauffeur['dateChauffeur']; ?></td>
  <td></td>

</tr>
</tbody>
<?php
}
?>

</table>


<h2>Historique Kilométriques</h2>
<table class="table table-striped">
<thead>
  <tr class="kilometrage">
    <th>Immatriculation</th>
    <th>Date relevé Kilométriques</th>
    <th>Kilométrage actualisé</th>
    <th></th>
  </tr>
</thead>   


  <?php
$req3 = $bdd->query("SELECT * FROM historiquekilometrage,vehicule WHERE vehicule.idVehicule = ".$idVehicule." AND vehicule.idVehicule = historiquekilometrage.idVehicule ORDER BY dateReleve");


while ($historiquekilometrage = $req3->fetch()) { 


?>
<tbody>
<tr>
  <td><?php echo $historiquekilometrage['immat']; ?></td>
  <td><?php echo $historiquekilometrage['dateReleve']; ?></td>
  <td><?php echo $historiquekilometrage['historiqueKilometre']; ?></td>
  <td></td> 

</tr>
</tbody>
<?php
}
?>

</table>


<h2>Historique Etat</h2>
<table class="table table-striped">
<thead>
  <tr class="etat">
    <th>Immatriculation</th>
    <th>Modèle</th>
    <th>Etat du véhicule</th>
    <th>Date depot véhicule</th>
    <th>Date recupération véhicule</th>
    <th></th>
  </tr>
</thead>   

<?php
$reqa = $bdd->query("SELECT * FROM etat,vehicule,vehiculeprobleme WHERE  vehicule.idVehicule = ".$idVehicule." AND etat.idEtat = vehiculeprobleme.idEtat AND vehiculeprobleme.idVehicule = vehicule.idVehicule ORDER BY dateDepot");


while ($historiqueetat = $reqa->fetch()) { 
if($historiqueetat ["idEtat"] == 3 || $historiqueetat ["idEtat"]==4 ){

?>
<tbody>
<tr>
  <td><?php echo $historiqueetat['immat']; ?></td>
  <td><?php echo $historiqueetat['modele']; ?></td>
  <td><?php echo $historiqueetat['nomEtat']; ?></td>
  <td><?php echo $historiqueetat['dateDepot']; ?></td>
  <td><?php echo $historiqueetat['dateRecup']; ?></td>
  <td></td> 

</tr>
</tbody>
<?php
}
}
}
?>

</table><br><br>


<div class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>
</form>
</body>

    <script src="../www/bootstrap/js/jquery.js"></script>
    <script src="../www/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../www/js/bootstrap.min.js"></script>
</html>