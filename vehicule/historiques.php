<?php  
session_start();
$connect = mysqli_connect("localhost", "root", "" , "tableaumktn");

  if ($_SESSION['id_connexion']==null) {
  header("Location: ../accueil/connexion.php");
}

$query= "SELECT * FROM vehicule,chauffeur, etat WHERE vehicule.idChauffeur = chauffeur.idChauffeur AND etat.idEtat = vehicule.idEtat";
$result = mysqli_query($connect,$query);


include('../PHPExcel/FichierExcel.php');
$fichier = new FichierExcel();

if(isset($_POST['export'])){
    $info = $_SESSION['donnees'];
    $fichier->Insertion($info);
    $fichier->output('Vehicule');
  }

/*$req = 'SELECT * FROM vehicule';
   $vehicule = $bdd->prepare($req);
   $vehicule->execute();
   $donneesVehicule = $vehicule->fetchAll(PDO::FETCH_ASSOC);*/
   
?>


<html>
<head>
  <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link rel="stylesheet" type="text/css" href="../js/datatables.css">
        <link rel="stylesheet" type="text/css" href="../js/DataTables-1.10.16/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
        <script type="text/javascript" charset="utf8" src="../js/datatables.js">
        </script>
        <script type="text/javascript" charset="utf8" src="../js/DataTables-1.10.16/js/dataTables.bootstrap.min.js">
        </script>
        <script type="text/javascript" charset="utf8" src="../js/DataTables-1.10.16/js/jquery.dataTables.min.js">
        </script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <script src="../js/jQuery.msgBox-master/scripts/jquery.msgBox.js" type="text/javascript"></script>
    <link href="../js/jQuery.msgBox-master/styles/msgBoxLight.css" rel="stylesheet" type="text/css">


   <title>Formulaire</title>
    <script type="text/javascript">
    function example3(id) {
            $.msgBox({
                title: "Supprimer",
                content: "Êtes vous sûr de vouloir supprimer ce véhicule ?",
                type: "error",
                buttons: [{ value: "Oui" }, { value: "Non" }, { value: "Annuler"}],
                success: function (result) {
                    if (result == "Oui") {
                        document.location.href="supprimerVehicule.php?id="+id;
                        
                    }
                }
            });
        }
    </script>
  </head>
<body class="index">
     <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>
     <?php
 include ("../menu/menu.php"); 
 
 $donnees = "Chauffeur ; Immatriculation ; Modèle ; Kilométrage actuelle ; Etat ; Date Réception ; Date de Retour"."\n";
 ?>
<center><h2>Informations du véhicule </h2></center>
<div class="container">
<div class="container">
<table class="display" id="example" ">
<thead>
  <tr class="vehicule">
    <th>Chauffeur</th>
    <th>Immatriculation</th>
    <th>Modèle</th>
    <th>Kilométrage actuelle</th>
    <th>Etat</th>
    <th>Date Réception</th>
    <th>Date de Retour</th>
    <th></th>

  </tr>
</thead>
<tbody>
  <?php

while ($vehicule = mysqli_fetch_array($result)){ 
$idv = $vehicule['idVehicule'];
?>

  <tr class="gradeX">
  <td><?php echo $vehicule['nomChauffeur']; $donnees = $donnees . $vehicule['nomChauffeur'].";"; ?></td>
  <td><?php echo $vehicule['immat']; $donnees = $donnees . $vehicule['immat'].";"; ?></td>
  <td><?php echo $vehicule['modele']; $donnees = $donnees . $vehicule['modele'].";"; ?></td>
  <td><?php echo $vehicule['kilomActuelle']; $donnees = $donnees . $vehicule['kilomActuelle'].";" ; ?></td>
  <td><?php echo $vehicule['nomEtat']; $donnees = $donnees . $vehicule['nomEtat'].";"; ?></td>
  <td><?php echo $vehicule['dateReception']; $donnees = $donnees . $vehicule['dateReception'].";"; ?></td>
  <td><?php echo $vehicule['dateRetour']; $donnees = $donnees . $vehicule['dateRetour'].";"; ?></td>
  <td><a href="#" title="Supprimer le vehicule" alt="delete" onClick="example3(<?php echo $vehicule['idVehicule']?>)">Supprimer</a></td>
  </tr>

<?php
}
?>
</tbody>

</table>
<form action="" method="POST">
    <button type="submit" name="export" class="btn btn-primary"><b>Exporter excel</b></button>
</form>

<script type="text/javascript">
$(document).ready(function () {

    $('#example').DataTable({

        language: {

            url: "../js/French.json"

        }

    });

});
</script>

<div border ="solid" class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>
<?php $_SESSION['donnees'] = $donnees; ?>
</body>

  
</html>