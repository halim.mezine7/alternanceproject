<?php
session_start();//On ouvre la session
$bdd = new PDO('mysql:host=localhost;dbname=tableaumktn;charset=utf8', 'root', ''); //connection à la bdd
$getid = $_GET['id'];

$vehicule = $bdd -> prepare ('SELECT * FROM vehicule WHERE idVehicule = ?');
$vehicule -> execute(array($getid));
$vehi = $vehicule -> fetch();

if (isset($_POST['newboutonconnect'])) {
  include('modifierVehicule.php');
}
?>
<html>
  <head>
  <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link href="../www/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
   <title>Formulaire</title>
  </head>
<body class="index">
     <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>
     <?php
 include ("../menu/menu.php"); 
 ?>
    <div class="formPosition">
>
    <form action="#" method="POST" class="form-horizontal">

        <center>
          <h1>Modification(s) des informations personnelles de <?php echo $vehi['modele']; ?></h1>
        </center>
        <br>

        <div class="form-group">
            <label class="control-label col-sm-4"></label>
              <div class="col-sm-4">
                  <b>Immatriculation :</b><input type="text" class="form-control" name="newImmat" value="<?php echo $vehi['immat']; ?>">
              </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4"></label>
              <div class="col-sm-4">
                  <b>Modele :</b><input type="text" class="form-control" name="newModele" value="<?php echo $vehi['modele']; ?>">
              </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4"></label>
              <div class="col-sm-4">
                  <b>Kilometres actuelle :</b><input type="text" class="form-control" name="newKilomActuelle" value="<?php echo $vehi['kilomActuelle']; ?>">
              </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4"></label>
              <div class="col-sm-4"> 
                   <b>Date de relevé kilométriques :</b><input type="date" class="form-control" name="newDateRel" value="<?php echo $vehi['dateReleve']; ?>">
              </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-4"></label>
                <div class="col-sm-4"> 
                    <b>Chauffeur :</b><input type="text" class="form-control" name="newChauffeur" value="<?php echo $vehi['nomChauffeur']; ?>">
                </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-4"></label>
                <div class="col-sm-4"> 
                    <b>Etat :</b><SELECT name="newEtat" type="text" class="form-control" size="1" value="<?php echo $vehi['etat']; ?>">
                      <OPTION>Rendu</OPTION>
                      <OPTION>En Possession</OPTION>
                      <OPTION>En Revision</OPTION>
                    </SELECT>
                </div>
       </div>

      
        <div class="form-group">
            <label class="control-label col-sm-4"></label>
                <div class="col-sm-4"> 
                    <b>Date de réception :</b><input type="date" class="form-control" name="newDateRec" value="<?php echo $vehi['dateReception']; ?>">
                </div>
        </div>

         <div class="form-group">
            <label class="control-label col-sm-4"></label>
                <div class="col-sm-4"> 
                    <b>Date de Retour :</b><input type="date" class="form-control" name="newDateRet" value="<?php echo $vehi['dateRetour']; ?>">
                </div>       
        </div>
          <center>
            
            <br><button type="submit" name="newboutonconnect" class="btn btn-primary"><b>Modifier le vehicule</b></button>
            
           
                 <?php 
                    if(isset($msg)){
                        echo $msg;
                    }
                ?>
          </center>
    </form>
    </div>

    </body>
</html>