<?php 
$bdd = new PDO('mysql:host=localhost;dbname=tableaumktn;charset=utf8', 'root', ''); 
session_start();
ini_set( 'display_errors', 'on' );
error_reporting( E_ALL );
$req = 'SELECT * FROM vehicule';
    $vehicule = $bdd->prepare($req);
    $vehicule->execute();

$req1 = 'SELECT * FROM  chauffeur';
  $chauffeur = $bdd->prepare($req1);
  $chauffeur->execute();
  $donneesChauffeur = $chauffeur->fetchAll(PDO::FETCH_ASSOC);

$req5 = 'SELECT * FROM etat';
  $etat = $bdd->prepare($req5);
  $etat->execute();
  $donneesEtat = $etat->fetchAll(PDO::FETCH_ASSOC);
 
  if (isset($_POST["bouton1"])) {

          $immat = htmlspecialchars(trim($_POST['immat']));
          $modele = htmlspecialchars(trim($_POST['modele']));
          $kilomActuelle = htmlspecialchars(trim($_POST['kilomActuelle']));
          $idChauffeur = htmlspecialchars(trim($_POST['idChauffeur']));
          $idEtat = 2;
          $dateReception = htmlspecialchars(trim($_POST['dateReception']));
          $dateRetour = htmlspecialchars(trim($_POST['dateRetour']));
          // $idVehicule = htmlspecialchars(trim($_POST['idVehicule']));

          $reqvehi = $bdd->prepare('INSERT INTO vehicule(immat, modele, kilomActuelle, idChauffeur, idEtat, dateReception, dateRetour) VALUES(:immat, :modele, :kilomActuelle, :idChauffeur, :idEtat, :dateReception, :dateRetour)');

        $reqvehi->execute(array(
     
              'immat' => $immat,
              'modele' => $modele,
              'kilomActuelle' => $kilomActuelle,
              'idChauffeur' => $idChauffeur,
              'idEtat'=> $idEtat,
              'dateReception' => $dateReception,
              'dateRetour' => $dateRetour
               ));
        $idVehicule = $bdd -> lastInsertId();
        $reqcha = $bdd->prepare('INSERT INTO historiqueChauffeur(idVehicule,idChauffeur, dateChauffeur) VALUES(:idVehicule, :idChauffeur, :dateChauffeur)');
        $reqcha->execute(array(
     
              'idVehicule' => $idVehicule,
              'idChauffeur' => $idChauffeur,
              'dateChauffeur' => $dateReception
               ));   

        $reqkilom = $bdd->prepare('INSERT INTO historiqueKilometrage(dateReleve, historiqueKilometre,idVehicule) VALUES(:dateReleve, :historiqueKilometre, :idVehicule)');
        $reqkilom->execute(array(
     
              'dateReleve' => $dateReception,
              'historiqueKilometre' => $kilomActuelle,
              'idVehicule' => $idVehicule
               ));    

   }

?>

<html>
<head>
  <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link href="../www/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  <title>Formulaire</title>
</head>
<body class="index">
     <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>

<?php include ("../menu/menu.php");

?>

  <form action="#" method="POST">

    <fieldset>
      <table>

          <legend style = "color : #156094">Ajout de Vehicule</legend>

        <tr>
          <th><label for="immat">Immatriculation : </label></th>
          <td><input type="text" name="immat"/><td>
        </tr>

           <tr><td>&nbsp</td></tr>

        <tr>
          <th><label for="modele">Modèle : </label></th>
          <td><input type="text" name="modele"/><td>
        </tr>
         
         <tr>
          <th><br><label for="kilomActuelle">Kilometrage Actuelle : </label></th>
          <td><br><input type="text" name="kilomActuelle"/></td>
         </tr>

        <tr>
          <th><br><label for="idChauffeur" >Nom du Chauffeur : </label></th>
          <td><br><SELECT name="idChauffeur" value ="idChauffeur" size="1" style="width: 200px; ">
            <?php
                  foreach ($donneesChauffeur AS $donneeChauffeur)
                  {
                    ?>
                  
                  <option value="<?php echo $donneeChauffeur['idChauffeur'];?>">
                  <?php echo $donneeChauffeur['nomChauffeur']; ?></option>
                  <?php
                  }
                  ?>
          </SELECT></td>
        </tr>

         <tr>
          <th><br><label for="dateReception">Date de Réception : </label></th>
          <td><br><input type="date" name="dateReception" style="width: 200px;"/></td>
           </tr>

        <tr>
            <th><br><label for="dateRetour">Date Retour Véhicule : </label></th>
            <td><br><input type="date" name="dateRetour" style="width: 200px;"/></td>
        </tr>

        <tr>
            <th></th>
            <td><br><button type="submit" name="bouton1" class="btn btn-primary"><b>Ajouter</b></button></td>  
        </tr>

      </table>
    </fieldset>
</form>


<div class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>
</body>
    <script src="../www/bootstrap/js/jquery.js"></script>
    <script src="../www/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../www/js/bootstrap.min.js"></script>
</html>