<?php
  $bdd = new PDO('mysql:host=localhost;dbname=tableaumktn;charset=utf8', 'root', ''); 
  session_start();
  $idVehicule = $_SESSION['idVehicule'];

  $reqchauff = 'SELECT * FROM  chauffeur';
      $chauffeur = $bdd->prepare($reqchauff);
      $chauffeur->execute();
      $donneesChauffeur = $chauffeur->fetchAll(PDO::FETCH_ASSOC);

  $reqvehic = 'SELECT * FROM vehicule';
      $vehicule = $bdd->prepare($reqvehic);
      $vehicule->execute();
      $donneesVehicule = $vehicule->fetchAll(PDO::FETCH_ASSOC);

  $reqetat = 'SELECT * FROM etat';
      $etat = $bdd->prepare($reqetat);
      $etat->execute();
      $donneesEtat = $etat->fetchAll(PDO::FETCH_ASSOC);

  $reqprob = 'SELECT * FROM vehiculeprobleme';
      $vehiculeprobleme = $bdd->prepare($reqprob);
      $vehiculeprobleme->execute();
      $donneesProbleme = $vehiculeprobleme->fetchAll(PDO::FETCH_ASSOC);



   $req7 = $bdd ->prepare ("SELECT * FROM vehicule, historiquechauffeur, historiquekilometrage,vehiculeprobleme, chauffeur WHERE vehicule.idVehicule = ".$idVehicule." AND vehicule.idVehicule =  historiquekilometrage.idVehicule AND vehicule.idVehicule = historiquechauffeur.idVehicule AND vehicule.idVehicule = vehiculeprobleme.idVehicule AND chauffeur.idChauffeur = vehicule.idChauffeur");
      $req7->execute();
      $infosVehicule = $req7->fetch();


      if (isset($_POST['boutonvehicule'])) {

      
          $idEtat = htmlspecialchars(trim($_POST['idEtat']));
          $dateDepot = htmlspecialchars(trim($_POST['dateDepot']));
          $dateRecup = htmlspecialchars(trim($_POST['dateRecup']));


          $reqv = $bdd->prepare('INSERT INTO vehiculeprobleme (idEtat, dateDepot, dateRecup, idVehicule) VALUES(:idEtat, :dateDepot, :dateRecup, :idVehicule)');  
          $reqv->execute(array(
          ':idEtat' => $idEtat,
          ':dateDepot' => $dateDepot,
          ':dateRecup' => $dateRecup,
          ':idVehicule' => $idVehicule
               ));

        // $reqetat = "UPDATE vehicule SET idEtat = ".$idEtat." WHERE idVehicule =".$idVehicule."";
         // $bdd ->exec($reqetat);

        // $reqrecep = "UPDATE vehicule SET dateReception = ".$dateReception." WHERE idVehicule =".$idVehicule."";
         // $bdd ->exec($reqrecep);

        // $reqretour = "UPDATE vehicule SET dateRetour  = ".$dateRetour." WHERE idVehicule =".$idVehicule."";
         // $bdd ->exec($reqretour);

       
        }
      if (isset($_POST['boutonchauffeur'])) {
         
          $idChauffeur = htmlspecialchars(trim($_POST['idChauffeur']));
          $dateChauffeur = htmlspecialchars(trim($_POST['dateChauffeur']));

        $reqc = $bdd->prepare('INSERT INTO historiquechauffeur(dateChauffeur,idChauffeur, idVehicule) VALUES(:dateChauffeur,:idChauffeur, :idVehicule)');
        
        $reqc->execute(array(
              'dateChauffeur' => $dateChauffeur,
              'idChauffeur' => $idChauffeur,
              'idVehicule' => $idVehicule
               ));
  
        $reqchauffeur = "UPDATE vehicule SET idChauffeur = ".$idChauffeur." WHERE idVehicule=".$idVehicule."";
        $bdd ->exec($reqchauffeur);


  
        }
      if (isset($_POST['boutonkilometrique'])) { 

        $historiqueKilometre = htmlspecialchars(trim($_POST['historiqueKilometre']));
        $dateReleve = htmlspecialchars(trim($_POST['dateReleve']));
  

        $reqk = $bdd->prepare('INSERT INTO historiquekilometrage (historiqueKilometre,dateReleve, idVehicule) VALUES(:historiqueKilometre, :dateReleve, :idVehicule)');
        
        $reqk->execute(array(
              'historiqueKilometre' => $historiqueKilometre,
              'dateReleve' => $dateReleve,
              'idVehicule' => $idVehicule
               ));

        $reqkilometre = "UPDATE vehicule SET kilomActuelle = ".$historiqueKilometre." WHERE idVehicule=".$idVehicule."";
         $bdd ->exec($reqkilometre);

    } 

?>
<html>
<head>
    <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link href="../www/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  <title>Formulaire</title>
</head>
<body class="index">
     <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>

<?php include ("../menu/menu.php") ?>

    <form action="#" method="POST">
      <div class="formvehicule">
      <fieldset>
      <table>
       <tr>
          <th><h2>Modification du Véhicule</h2></th>

        <tr>
          <th><br><label for="idEtat">Etat du véhicule : </label></th>
          <td><br><SELECT name="idEtat" value ="idEtat" size="1" style="width: 150px; ">
            <?php
                  foreach ($donneesEtat AS $donneeEtat)
                  {
                    ?>
                  
                  <option value="<?php echo $donneeEtat['idEtat'];?>"<?php if ($donneeEtat['idEtat'] == $infosVehicule['idEtat']) { echo "selected"; } ?>>
                  <?php echo $donneeEtat['nomEtat']; ?></option>
                  <?php
                  }
                  ?>
          </SELECT></td>
        </tr>

        <tr>
            <th><br><label for="dateDepot">Date depot véhicule : </label></th>
            <td><br><input type="date"  style="width: 150px" name="dateDepot" /></td>
           </tr>

        <tr>
            <th><br><label for="dateRecup">Date récupération véhicule : </label></th>
            <td><br><input type="date"  style="width: 150px" name="dateRecup"/></td>
      </tr>

        <tr>
          <th></th>
          <td><br><button type="submit" name="boutonvehicule" class="btn btn-primary"><b>Modifier</b></button></td>
        </tr>
        
      </table>   
      </fieldset>
      </div>
    </form>


    <form action="#" method="POST">
      <div class="formchauffeur">
      <fieldset>
      <table>

        <tr>
            <th><h2>Modification du chauffeur</h2></th>
        </tr>
       
        <tr>
          <th><br><label for="idChauffeur">Nom du chauffeur : </label></th>
          <td><br><SELECT name="idChauffeur" value ="idChauffeur" size="1" style="width: 150px; ">
            <?php
                  foreach ($donneesChauffeur AS $donneeChauffeur)
                  {
                    ?>
                  
                  <option value="<?php echo $donneeChauffeur['idChauffeur'];?>"<?php if ($donneeChauffeur['idChauffeur'] == $infosVehicule['idChauffeur']) { echo "selected"; } ?>>
                  <?php echo $donneeChauffeur['nomChauffeur']; ?></option>
                  <?php
                  }
                  ?>
          </SELECT></td>
        </tr>

        <tr>
          <th><br><label for="dateChauffeur">Date changement de chauffeur : </label></th>
          <td><br><input type="date" name="dateChauffeur" value="<?php echo $histochauffeur['dateChauffeur'];?>" style="width: 150px; "/></td>
        </tr>

         <tr>
          <th></th>
          <td><br><button type="submit" name="boutonchauffeur" class="btn btn-primary"><b>Modifier</b></button></td>
        </tr>
      </table>   
      </fieldset>
      </div>
    </form>


    <form action="#" method="POST">
      <div class="formkilometrage">
      <fieldset>
      <table>

        <tr>
          <th><h2>Modification du kilométrage</h2></th>
        </tr>

        <tr>
          <th><br><label for="historiqueKilometre">Nouveau kilométrage : </label></th>
          <td><br><input type="text" name="historiqueKilometre" style="width: 150px"></td>
        </tr> 

        <tr>
          <th><br><label for="dateReleve">Date de relevé kilométriques : </label></th>
          <td><br><input type="date" name="dateReleve" value="<?php echo $histokilometrage['dateReleve'];?>" style="width: 150px; "/></td>
        </tr> 

        <tr>
          <th></th>
          <td><br><button type="submit" name="boutonkilometrique" class="btn btn-primary"><b>Modifier</b></button></td>
        </tr>
      
      </table>   
      </fieldset>
      </div>
    </form>

<br><br><br>
<div class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>
</body>
    <script src="../www/bootstrap/js/jquery.js"></script>
    <script src="../www/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../www/js/bootstrap.min.js"></script>
</html>
