<?php 
session_start();//On ouvre la session
$getid = $_GET['id'];

      if(isset($_POST['newImmat'])AND !empty($_POST['newImmat']) AND $_POST['newImmat'] != $vehi['immat'])
      {
        $newImmat = htmlspecialchars($_POST['newImmat']);
        $insertImmat = $bdd -> prepare("UPDATE vehicule SET immat = ? WHERE idVehicule = ?");
        $insertImmat -> execute(array($newImmat, $getid));
        header('Location: historiques.php?id='.$getid);
      }

      if(isset($_POST['newModele'])AND !empty($_POST['newModele']) AND $_POST['newModele'] != $vehi['modele'])
      {
        $newModele = htmlspecialchars($_POST['newModele']);
        $insertModele = $bdd -> prepare("UPDATE vehicule SET modele = ? WHERE idVehicule = ?");
        $insertModele -> execute(array($newModele, $getid));
        header('Location: historiques.php?id='.$getid);
      }

      if(isset($_POST['newKilomActuelle'])AND !empty($_POST['newKilomActuelle']) AND $_POST['newKilomActuelle'] != $vehi['kilomActuelle'])
      {
        $newKilomActuelle = htmlspecialchars($_POST['newKilomActuelle']);
        $insertKilom = $bdd -> prepare("UPDATE vehicule SET kilomActuelle = ? WHERE idVehicule = ?");
        $insertKilom -> execute(array($newKilomActuelle, $getid));
        header('Location: historiques.php?id='.$getid);
      }
      
        if(isset($_POST['newChauffeur'])AND !empty($_POST['newChauffeur']) AND $_POST['newChauffeur'] != $vehi['nomChauffeur'])
      {
        $newChauffeur = htmlspecialchars($_POST['newChauffeur']);
        $insertChauffeur = $bdd -> prepare("UPDATE vehicule SET nomChauffeur = ? WHERE idVehicule = ?");
        $insertChauffeur -> execute(array($newChauffeur, $getid));
        header('Location: historiques.php?id='.$getid);
      }
        if(isset($_POST['newEtat'])AND !empty($_POST['newEtat']) AND $_POST['newEtat'] != $vehi['etat'])
      {
        $newEtat = htmlspecialchars($_POST['newEtat']);
        $insertEtat = $bdd -> prepare("UPDATE vehicule SET etat = ? WHERE idVehicule = ?");
        $insertEtat -> execute(array($newEtat, $getid));
        header('Location: historiques.php?id='.$getid);
      }

        if(isset($_POST['newDateRel'])AND !empty($_POST['newDateRel']) AND $_POST['newDateRel'] != $vehi['dateReleve'])
      {
        $newDateRel = htmlspecialchars($_POST['newDateRel']);
        $insertDateRec = $bdd -> prepare("UPDATE historiqueKilometrage SET dateReleve = ? WHERE idVehicule = ?");
        $insertDateRec -> execute(array($newDateRel, $getid));
        header('Location: historiques.php?id='.$getid);
      }
        if(isset($_POST['newDateRec'])AND !empty($_POST['newDateRec']) AND $_POST['newDateRec'] != $vehi['dateReception'])
      {
        $newDateRec = htmlspecialchars($_POST['newDateRec']);
        $insertDateRec = $bdd -> prepare("UPDATE vehicule SET dateReception = ? WHERE idVehicule = ?");
        $insertDateRec -> execute(array($newDateRec, $getid));
        header('Location: historiques.php?id='.$getid);
      }
        if(isset($_POST['newDateRet'])AND !empty($_POST['newDateRet']) AND $_POST['newDateRet'] != $vehi['dateRetour'])
      {
        $newDateRet = htmlspecialchars($_POST['newDateRet']);
        $insertDateRet = $bdd -> prepare("UPDATE vehicule SET dateRetour = ? WHERE idVehicule = ?");
        $insertDateRet -> execute(array($newDateRet, $getid));
        header('Location: historiques.php?id='.$getid);
      }


