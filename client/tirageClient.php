<?php 
session_start();
  $connect = mysqli_connect("localhost", "root", "", "tableaumktn");
if ($_SESSION['id_connexion']==null) {
  header("Location: ../accueil/connexion.php");
}

  include('../PHPExcel/FichierExcel.php');
  $fichier = new FichierExcel();
  
  if(isset($_POST['export'])){
    $info = $_SESSION['donnees'];
    $fichier->Insertion($info);
    $fichier->output('Tirage client');
  }

$req = 'SELECT * FROM client';
    $result = mysqli_query($connect, $req);
    $donneesClient = mysqli_fetch_all($result,MYSQLI_ASSOC);
    mysqli_free_result($result);
    
?>

<head>
  <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link rel="stylesheet" type="text/css" href="../js/datatables.css">
        <link rel="stylesheet" type="text/css" href="../js/DataTables-1.10.16/css/dataTables.bootstrap.min.js">
        <link href="../www/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
        <script type="text/javascript" charset="utf8" src="../js/datatables.js">
        </script>
        <script type="text/javascript" charset="utf8" src="../js/DataTables-1.10.16/js/dataTables.bootstrap.min.js">
        </script>
        <script type="text/javascript" charset="utf8" src="../js/DataTables-1.10.16/js/jquery.dataTables.min.js">
        </script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
        <title>Tirage du client</title>
</head>

<body class="index">
     <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>
  <?php include ("../menu/menu.php");
  if (!isset($_POST["bouton"])) {
?>

      <form action="#" method="POST">
          <fieldset>
            <table>
              <legend style="color : #156094">Selectionner un client</legend>

            <div class="form-group"> 
            <tr>
                <th><label for="idTouret">Client : </label></th>
                <td><SELECT value="idClient" name="idClient" type="text" size="1">
                  
                  <?php
                  foreach ($donneesClient AS $donneeClient)
                  {
                    ?>
                  
                  <option value="<?php echo $donneeClient['idClient'];?>"<?php if (isset($_SESSION["idClient"])) {
                    if ($_SESSION["idClient"]==$donneeClient['idClient']) {echo "selected";} }?>>
                    <?php echo $donneeClient['nomClient']; ?> </option>

                  <?php
                   }
                  
                  ?>
                </SELECT></td>
            </tr>
            </div>
            <tr>
              <th></th>
              <td><br><button type="submit" name="bouton" class="btn btn-primary"><b>Choisir</b></button></td>
            </tr>
          </table>
    </fieldset>
  </form>
<?php  }   ?>

  <?php 
    if (isset($_POST["bouton"])) {
      $_SESSION['idClient'] = $_POST['idClient'];
?>
      <form action="#" method="POST" class="ecriture">
    <fieldset>
          <legend style = "color : #156094">Selectionner un client</legend>

            <div class="form-group"> 
            
                Client : 
                <SELECT value="idClient" name="idClient" type="text" size="1">
                  
                  <?php
                  foreach ($donneesClient AS $donneeClient)
                  {
                    ?>
                <option value="<?php echo $donneeClient['idClient'];?>"<?php if ($_SESSION["idClient"]==$donneeClient['idClient']) {echo "selected";} ?>>
                  <?php echo $donneeClient['nomClient']?> </option>

                  <?php
                  }
                  ?>
              </SELECT>

            </div>

              <button type="submit" name="bouton" class="btn btn-primary"><b>Choisir</b></button>

    </fieldset>
  </form>
  <?php
      $idClient = $_POST['idClient'];
        $query = "SELECT * FROM touret, projet, client, historique WHERE historique.idProjet = projet.idProjet AND client.idClient = historique.idClient AND historique.idTouret = touret.idTouret AND client.idClient = ".$idClient." ORDER BY idHistorique";
  $result = mysqli_query($connect, $query);

  $donnees = "Longueur Pose ; Date ; Touret ; Devis ; Code ; Projet"."\n";
?>
<center><h2> Affichage du tirage du client </h2></center>
<div class="container">
<div class="container">
<table class="display" id="example">

  <thead> 
    <tr class="cable">
    <th>Longueur Pose</th>
    <th>Date</th>
    <th>Touret</th>
    <th>Devis</th>
    <th>Code</th>
    <th>Projet</th>
  </tr>
  </thead>
  
  <tbody>
  <?php 
  $longueur = 0;
    while($row = mysqli_fetch_array($result)){
      $longueur = $row['longueurPose'] + $longueur;
  ?> 
    <tr class="gradeX"> 
      <td><?php echo $row['longueurPose']; $donnees = $donnees . $row['longueurPose'].";"; ?></td>
      <td><?php echo $row['dateJour']; $donnees = $donnees . $row['dateJour'].";"; ?></td>
      <td><?php echo $row['nomTouret']; $donnees = $donnees . $row['nomTouret'].";"; ?></td>
      <td><?php echo $row['devis']; $donnees = $donnees . $row['devis'].";"; ?></td>
      <td><?php echo $row['code']; $donnees = $donnees . $row['code'].";"; ?></td>
      <td><?php echo $row['typeProjet']; $donnees = $donnees . $row['typeProjet']."\n"; ?></td>
    </tr> 
  <?php } ?>
  <tr class="gradeX"> 
      <td><?php echo '= '.$longueur; $donnees = $donnees .$longueur."\n"; ?></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>

</table>
<form action="" method="POST">
    <button type="submit" name="export" class="btn btn-primary"><b>Exporter excel</b></button>
</form>

<?php
  $_SESSION['donnees'] = $donnees;
 } ?>

<script type="text/javascript">
$(document).ready(function () {

    $('#example').DataTable({

        language: {

            url: "../js/French.json"

        }

    });

});
</script>
<div border="solid" class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>
</body>
</div>