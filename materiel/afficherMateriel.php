<?php 
session_start();
$connect = mysqli_connect("localhost", "root", "", "tableaumktn");
$query ="SELECT * FROM materiel ";
$result = mysqli_query($connect, $query);  
?>
<head>
  <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css"/>
        <link rel="stylesheet" href="../css/menu.css"/>
        <link rel="stylesheet" type="text/css" href="../js/datatables.css">
        <link rel="stylesheet" type="text/css" href="../js/DataTables-1.10.16/css/dataTables.bootstrap.min.js">
        <link href="../www/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
        <script type="text/javascript" charset="utf8" src="../js/datatables.js">
        </script>
        <script type="text/javascript" charset="utf8" src="../js/DataTables-1.10.16/js/dataTables.bootstrap.min.js">
        </script>
        <script type="text/javascript" charset="utf8" src="../js/DataTables-1.10.16/js/jquery.dataTables.min.js">
        </script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  <title>Affichage des matériels</title>
 </head>
<body class="index">
     <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>
  <?php include ("../menu/menu.php");
  ?>

 <center><h2> Affichage des matériels</h2></center>
 <div class="container">
 <div class="container">
 <table class="display" id="example">

<thead>
  <tr class="materiel">
    <th>Nom du matériel </th>
    <th>Quantité(s)</th>
    <th><?php if($_SESSION['id_droit'] == 1 || $_SESSION['id_droit'] == 2){?><a href="#"></th><?php }?>
    <th><?php if($_SESSION['id_droit'] == 1 || $_SESSION['id_droit'] == 2){?><a href="#"></th><?php }?>
    <!-- <th></th> -->
  </tr>
</thead>

 <tbody>
      <?php
      while ($materiel = mysqli_fetch_array($result)) { 
       
?>
 
<tr class="gradeX">
  <td><?php echo $materiel['libelleMateriel']; ?></td>
  <td><?php echo $materiel['nbDeMateriel']; ?></td>
  <td><?php if($_SESSION['id_droit'] == 1 || $_SESSION['id_droit'] == 2){?><a href="retirerMateriel.php?id=<?php echo $materiel ['idMateriel']; ?>">Retirer</a></td><?php }?>
  <td><?php if($_SESSION['id_droit'] == 1 || $_SESSION['id_droit'] == 2){?><a href="rajouterMateriel.php?id=<?php echo $materiel ['idMateriel']; ?>">Ajouter</a></td><?php }?>
   <?php
    }
    ?>
  <!-- <td><a href="supprimerMateriel.php?id=<?php  /*echo $materiel['idMateriel']; */?>">Supprimer</a></td> -->
 </tr>

  </tbody>


</table>
<script type="text/javascript">
$(document).ready(function () {

    $('#example').DataTable({

        language: {

            url: "../js/French.json"

        }

    });

});
</script>
<div border="solid" class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>
</body>