<?php 
$bdd = new PDO('mysql:host=localhost;dbname=tableaumktn;charset=utf8', 'root', ''); 
session_start();

ini_set( 'display_errors', 'on' );
error_reporting( E_ALL );
$getId = intval($_GET['id']);
    $materiel = $bdd->prepare('SELECT * FROM materiel WHERE idMateriel = ?');
    $materiel->execute(array($getId));
    $donneesMateriel = $materiel->fetch(PDO::FETCH_ASSOC);

if (isset($_POST['boutonmat'])) {

        //Insertion des données dans la bdd
          // $libelleMateriel = htmlspecialchars(trim($_POST['libelleMateriel']));
          // $nbDeMateriel = htmlspecialchars(trim($_POST['nbDeMateriel']));
          $newNbMateriel = htmlspecialchars(trim($_POST['newNbMateriel']));

        $nbDeMateriel = $donneesMateriel['nbDeMateriel'];

        $nbDeMaterielRestants = $nbDeMateriel - $newNbMateriel;

        $reqModif = ("UPDATE materiel SET nbDeMateriel = ".$nbDeMaterielRestants." WHERE idMateriel = ".$getId."");
        $bdd ->exec($reqModif);
        echo$reqModif;?>
        <script type="text/javascript">
   alert($reqModif);
</script>

<?php
    } 

?>

<html>
<head>
    <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link href="../www/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  <title>Retrait de matériel</title>
</head>
<body class="index">
     <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>

<?php include ("../menu/menu.php") ?>
  <form action="#" method="POST">

    <fieldset>
      <table>
          <legend style = "color : #156094">Choisir le matériel à retirer</legend>
        <tr>
          <th><br><label for="materiel">Le matériel choisi : </label></th>
          <td><br>
            <?php echo $donneesMateriel['libelleMateriel']; ?>          
          </td>
        </tr>


        <tr>
          <th><br><label for="newNbMateriel" >Nombres de matériels choisis : </label></th>
          <td><br><input  name="newNbMateriel" style="width: 150px; "/></td>
        </tr>
       
        <tr>
          <th></th>
          <td><br><button type="submit" name="boutonmat" class="btn btn-primary"><b>Retirer du Matériel</b></button></td>
        </tr>
      </table>   
      </fieldset>
  </form>

<div class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>
</body>
    <script src="../www/bootstrap/js/jquery.js"></script>
    <script src="../www/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../www/js/bootstrap.min.js"></script>
</html>
