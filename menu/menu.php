<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="color : #156094">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span> 
                </button>
                  <a class ="navbar-brand" href="../accueil/index.php">Accueil</a>
                </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <!-- PM  -->
                        <!--<?php                    
                        // if($_SESSION['id_droit'] == 1 || $_SESSION['id_droit'] == 2){
                        ?>
                        <li style="text-align: center" class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">PM</a>
                                    <ul class="dropdown-menu" style="background: -webkit-linear-gradient( right, steelblue,white, grey)">
                            	        <li><a href="../PM/creationTableau.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey)">Création PM</a></li>
                                        <li><a href="../PM/remplirTableau.php" title="remplirPM"  style="background: -webkit-linear-gradient( right, steelblue,white, grey)"> Remplir PM</a></li>
                                        <li><a href="../PM/afficherTableau.php" title="afficherTableau" style="background: -webkit-linear-gradient( right, steelblue,white, grey);"> Afficher PM</a></li>
                                    </ul>
                        </li>-->

                           <?php 
                           // }                   
                        if($_SESSION['id_droit'] == 1 || $_SESSION['id_droit'] == 2){
                        ?>

                        <li style="text-align: center" class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Plan Boite</a>
                                    <ul class="dropdown-menu" style="background: -webkit-linear-gradient( right, steelblue,white, grey)">
                                        <li><a href="../Boite/planboite.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey)">Création Boite</a></li>
                                        <li><a href="../Boite/afficherplanboite.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">Afficher Boite</a></li>
                                    </ul>
                        </li>

                    <?php }?>

                        <li style="text-align: center" class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tourets</a>
                                    <ul class="dropdown-menu" style="background: -webkit-linear-gradient( right, steelblue,white, grey)"> 
                                                               
                                        <li><a href="../touret/creerTouret.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Création Touret(s)</a></li>
                
                                        <li><a href="../touret/remplirTouret.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Remplir Touret(s)</a></li>

                                        <li><a href="../touret/historiqueTouret.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Historique Touret(s)</a></li>
                                    </ul>
                        </li>


                        <li style="text-align: center" class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="afficherStock.php">Stocks</a>
                                    <ul class="dropdown-menu" style="background: -webkit-linear-gradient( right, steelblue,white, grey)">
                                        <li><a href="../stock/afficherStock.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Afficher Stock(s)</a></li>
                                        <li><a href="../stock/afficherStockRendu.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Afficher Stock(s) Rendu(s)</a></li>
                                    </ul>
                        </li>


                       <?php                  
                        if($_SESSION['id_droit'] == 1 || $_SESSION['id_droit'] == 2){
                        ?>

                        <li style="text-align: center" class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Clients</a>
                                    <ul class="dropdown-menu" style="background: -webkit-linear-gradient( right, steelblue,white, grey)">
                                        <li><a href="../client/ajouterClient.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Ajouter Client(s)</a></li>
                                        <li><a href="../client/afficherClient.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Afficher Client(s)</a></li>
                                        <li><a href="../client/tirageClient.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Tirage Client(s)</a></li>
                                    </ul>
                        </li>


                           <?php  }                 
                        if($_SESSION['id_droit'] == 1 || $_SESSION['id_droit'] == 2){
                        ?>  

                        <li style="text-align: center" class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Projets</a>
                                   <ul class="dropdown-menu" style="background: -webkit-linear-gradient( right, steelblue,white, grey)">
                                        <li><a href="../projet/ajouterProjet.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Ajouter Projet(s)</a></li>
                                        <li><a href="../projet/afficherProjet.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Afficher Projet(s)</a></li>
                                        <li><a href="../projet/tirageProjet.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Tirage Projet(s)</a></li>
                                    </ul>
                        </li>

                           <?php   }                 
                        if($_SESSION['id_droit'] == 1 || $_SESSION['id_droit'] == 2){
                        ?>

                        <li style="text-align: center" class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Stockages</a>
                                   <ul class="dropdown-menu" style="background: -webkit-linear-gradient( right, steelblue,white, grey)">
                                        <li><a href="../stockages/ajouterStockage.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Ajouter Stockage(s)</a></li>
                                        <li><a href="../stockages/afficherStockage.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Afficher Stockage(s)</a></li>
                                    </ul>
                        </li>

                    <?php }?>

                         <li style="text-align: center" class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Matériels</a>
                                   <ul class="dropdown-menu" style="background: -webkit-linear-gradient( right, steelblue,white, grey)">
                            <?php                    
                        if($_SESSION['id_droit'] == 1 || $_SESSION['id_droit'] == 2){
                        ?>
                                        <li><a href="../materiel/ajouterMateriel.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Ajouter Matériel(s)</a></li>
                                    <?php } ?>
                                        <li><a href="../materiel/afficherMateriel.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Afficher Matériel(s)</a></li>
                                    </ul>    

                           <?php                  
                        if($_SESSION['id_droit'] == 1 || $_SESSION['id_droit'] == 2){
                        ?>

                        <li style="text-align: center" class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Chauffeurs</a>
                                   <ul class="dropdown-menu" style="background: -webkit-linear-gradient( right, steelblue,white, grey)">
                                        <li><a href="../chauffeur/ajouterChauffeur.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Ajouter Chauffeur(s)</a></li>
                                        <li><a href="../chauffeur/afficherChauffeur.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Afficher Chauffeur(s)</a></li>
                                    </ul>    


                           <?php }

                            if($_SESSION['id_droit'] == 1 || $_SESSION['id_droit'] == 2){
                        ?>

                        <li style="text-align: center" class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Rapports</a>
                                    <ul class="dropdown-menu" style="background: -webkit-linear-gradient( right, steelblue,white, grey)">
                                        <li><a href="../rapport/redactionRapport.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Rédaction du Rapport</a></li>
                                        <li><a href="../rapport/afficherRapport.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Voir les rapports </a></li>
                                    </ul>

                            <?php }

                        if($_SESSION['id_droit'] == 1 || $_SESSION['id_droit'] == 2){
                        ?>

                        <li style="text-align: center" class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Véhicules</a>
                                    <ul class="dropdown-menu" style="background: -webkit-linear-gradient( right, steelblue,white, grey)">
                                        <li><a href="../vehicule/ajouterVehicule.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Ajouter Véhicule(s)</a></li>
                                        <li><a href="../vehicule/remplirVehicule.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Modifier les informations Véhicule(s) </a></li>
                                        <li><a href="../vehicule/historiques.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                        Informations des Véhicules</a></li>
                                        <li><a href="../vehicule/historiquesafficher.php" style="background: -webkit-linear-gradient( right, steelblue,white, grey);">
                                         Historiques</a></li>
                                    </ul>

                        <?php } if (isset($_SESSION['id_connexion'])) { ?>

                        <li style="text-align: center" class="dropdown">
                            <a class="dropdown-toggle" href="../accueil/deconnexion.php">Déconnexion</a>
                        </li>

                        <?php } ?>

                    </ul>
 			</div>
</nav> 				