-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 19 nov. 2018 à 09:52
-- Version du serveur :  10.1.30-MariaDB
-- Version de PHP :  7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tableaumktn`
--

-- --------------------------------------------------------

--
-- Structure de la table `cablesortie`
--

CREATE TABLE `cablesortie` (
  `idCable` int(20) NOT NULL,
  `numCable` int(50) NOT NULL,
  `idPA` int(11) NOT NULL,
  `numPB` int(50) NOT NULL,
  `capaCable` varchar(50) NOT NULL,
  `TRcable` int(20) NOT NULL,
  `nbTube` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `cablesortie`
--

INSERT INTO `cablesortie` (`idCable`, `numCable`, `idPA`, `numPB`, `capaCable`, `TRcable`, `nbTube`) VALUES
(9, 0, 4, 0, '36', 0, 30),
(10, 0, 4, 0, '24', 0, 18),
(11, 0, 4, 0, '6', 0, 6),
(12, 0, 4, 0, '12', 0, 12),
(13, 1, 4, 1, '36', 150120, 30),
(14, 2, 4, 2, '36', 150124, 36);

-- --------------------------------------------------------

--
-- Structure de la table `chauffeur`
--

CREATE TABLE `chauffeur` (
  `idChauffeur` int(10) NOT NULL,
  `nomChauffeur` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `chauffeur`
--

INSERT INTO `chauffeur` (`idChauffeur`, `nomChauffeur`) VALUES
(1, 'Halim'),
(2, 'Antoine'),
(4, 'kkkkkkkkkk'),
(6, 'Marvin'),
(7, 'Issa'),
(8, 'Kevin'),
(9, 'Ayoub'),
(10, 'Toufik'),
(11, 'Hôtel F1'),
(12, 'Romain'),
(13, 'Marc'),
(14, 'Yahya'),
(15, 'Gerard'),
(16, 'Nicolas');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `idClient` int(20) NOT NULL,
  `nomClient` varchar(55) NOT NULL,
  `adresseClient` varchar(55) NOT NULL,
  `tel` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`idClient`, `nomClient`, `adresseClient`, `tel`) VALUES
(10, 'SOGETREL', '', 0),
(11, 'ERT', '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `connexion`
--

CREATE TABLE `connexion` (
  `id_connexion` int(50) NOT NULL,
  `login` varchar(50) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  `id_droit` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `connexion`
--

INSERT INTO `connexion` (`id_connexion`, `login`, `mdp`, `id_droit`) VALUES
(1, 'admin', 'admin', '1'),
(2, 'bureau', 'bureau', '2'),
(3, 'tech', 'tech', '3'),
(4, 'visiteur', 'visiteur', '4');

-- --------------------------------------------------------

--
-- Structure de la table `droits`
--

CREATE TABLE `droits` (
  `id_droit` int(11) NOT NULL,
  `libelle_droit` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `droits`
--

INSERT INTO `droits` (`id_droit`, `libelle_droit`) VALUES
(1, 'Admin'),
(2, 'Bureau d\'études'),
(3, 'Technicien'),
(4, 'Visiteur');

-- --------------------------------------------------------

--
-- Structure de la table `etat`
--

CREATE TABLE `etat` (
  `idEtat` int(20) NOT NULL,
  `nomEtat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `etat`
--

INSERT INTO `etat` (`idEtat`, `nomEtat`) VALUES
(1, 'Rendu'),
(2, 'En Possession'),
(3, 'En Revision '),
(4, 'En Panne');

-- --------------------------------------------------------

--
-- Structure de la table `fibre`
--

CREATE TABLE `fibre` (
  `idFibre` int(20) NOT NULL,
  `numFibre` int(20) NOT NULL,
  `PA` varchar(50) NOT NULL,
  `TR` varchar(50) NOT NULL,
  `PT` varchar(50) NOT NULL,
  `idPM` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `fibre`
--

INSERT INTO `fibre` (`idFibre`, `numFibre`, `PA`, `TR`, `PT`, `idPM`) VALUES
(117, 261, '00025', '150126', '000129', 1),
(118, 262, '00025', '150126', '000129', 1),
(119, 263, '00025', '150126', '000129', 1),
(120, 264, '00025', '150126', '000129', 1),
(121, 265, '00025', '150127', '000130', 1),
(122, 266, '00025', '150127', '000130', 1),
(123, 267, '00025', '150127', '000130', 1),
(124, 268, '00025', '150127', '000130', 1),
(125, 269, '00025', '150127', '000130', 1),
(126, 270, '00025', '150127', '000130', 1),
(127, 271, '00025', '150128', '000131', 1),
(128, 272, '00025', '150128', '000131', 1),
(129, 273, '00025', '150128', '000131', 1),
(130, 274, '00025', '150128', '000131', 1),
(131, 275, '00025', '150128', '000131', 1),
(132, 276, '00025', '150128', '000131', 1);

-- --------------------------------------------------------

--
-- Structure de la table `historique`
--

CREATE TABLE `historique` (
  `idHistorique` int(20) NOT NULL,
  `longueurPose` varchar(55) NOT NULL,
  `longueurDispo` varchar(55) NOT NULL,
  `devis` varchar(55) NOT NULL,
  `dateJour` date NOT NULL,
  `code` int(20) NOT NULL,
  `idClient` int(20) NOT NULL,
  `idProjet` int(20) NOT NULL,
  `idTouret` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `historique`
--

INSERT INTO `historique` (`idHistorique`, `longueurPose`, `longueurDispo`, `devis`, `dateJour`, `code`, `idClient`, `idProjet`, `idTouret`) VALUES
(1, '600', '', '123456', '2018-04-11', 2275, 0, 0, 1),
(2, '200', '', '321654', '2018-04-11', 2275, 0, 0, 1),
(4, '200', '', '123654', '2018-05-23', 132, 10, 27, 1),
(5, '600', '', '9784', '2018-05-23', 321654, 10, 27, 11),
(6, '80', '', '8979', '2018-05-30', 789789, 11, 29, 8),
(7, '80', '', '8979', '2018-05-30', 789789, 11, 29, 8),
(8, '200', '', '123456', '2018-05-18', 0, 11, 29, 19),
(9, '2350', '', '256', '2018-05-30', 92444, 11, 30, 19);

-- --------------------------------------------------------

--
-- Structure de la table `historiquechauffeur`
--

CREATE TABLE `historiquechauffeur` (
  `idHistoriqueC` int(20) NOT NULL,
  `dateChauffeur` date NOT NULL,
  `idVehicule` int(11) NOT NULL,
  `idChauffeur` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `historiquechauffeur`
--

INSERT INTO `historiquechauffeur` (`idHistoriqueC`, `dateChauffeur`, `idVehicule`, `idChauffeur`) VALUES
(143, '2018-06-06', 90, 6),
(148, '2018-05-19', 95, 16),
(151, '2018-06-06', 90, 12),
(152, '2018-06-05', 96, 8),
(153, '2018-06-01', 97, 9),
(154, '2018-06-07', 98, 6),
(155, '2018-06-14', 99, 13),
(157, '2018-06-23', 101, 10),
(158, '2018-06-06', 102, 7);

-- --------------------------------------------------------

--
-- Structure de la table `historiquekilometrage`
--

CREATE TABLE `historiquekilometrage` (
  `idHistoriqueK` int(11) NOT NULL,
  `dateReleve` date NOT NULL,
  `historiqueKilometre` varchar(50) NOT NULL,
  `idVehicule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `historiquekilometrage`
--

INSERT INTO `historiquekilometrage` (`idHistoriqueK`, `dateReleve`, `historiqueKilometre`, `idVehicule`) VALUES
(83, '2018-06-06', '65000', 90),
(88, '2018-05-19', '77888', 95),
(89, '2018-06-05', '15000', 96),
(90, '2018-06-01', '45000', 97),
(91, '2018-06-07', '12500', 98),
(92, '2018-06-14', '45000', 99),
(94, '2018-06-23', '18654', 101),
(95, '2018-06-06', '67987', 102);

-- --------------------------------------------------------

--
-- Structure de la table `materiel`
--

CREATE TABLE `materiel` (
  `idMateriel` int(20) NOT NULL,
  `libelleMateriel` varchar(50) NOT NULL,
  `nbDeMateriel` int(50) NOT NULL,
  `newNbMateriel` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `materiel`
--

INSERT INTO `materiel` (`idMateriel`, `libelleMateriel`, `nbDeMateriel`, `newNbMateriel`) VALUES
(1, 'Scotch (*10)', 8, 0),
(2, 'Boite(s) Blackbox', 17, 10),
(3, 'Petite boite Fosc', 53, 0),
(4, 'Tiroir', 2, 0),
(5, 'Bombe de gaz', 20, 0),
(6, 'RAD', 12, 1),
(7, 'Attaches chevilles', 3, 0),
(8, 'Couvre chaussures', 200, 0),
(9, 'Ficelle', 10, 0),
(10, 'Jarretiere(s) optique(s)', 20, 2),
(11, 'Cassettes', 7, 0),
(12, 'Embase cheville *100', 2, 0),
(13, 'Collier d\'installation', 280, 0),
(15, 'Colson (Grand)', 10, 0),
(16, 'Colson (Moyen)', 127, 0),
(17, 'Colson Blanc', 41, 0),
(18, 'Etiquette(s) blanche(s) *100', 2, 0),
(19, 'Etiquette(s) verte(s)', 12, 0),
(20, 'Etiquette(s) orange(s)', 7, 0),
(21, 'Etiquette(s) bleue(s)', 2, 0),
(22, 'Gaine(s) Orange(s)', 5, 0),
(23, 'Gaine(s) blanche(s)', 3, 0),
(24, 'Gaine(s) verte(s)', 1, 0),
(25, 'Gaine(s) bleue(s)', 20, 15),
(26, 'Colson 250mm *100', 19, 0),
(27, 'Bombe Fluo', 3, 0),
(28, 'PBO', 2, 0),
(29, 'Sangle', 5, 0),
(30, 'Combustible', 2, 0),
(32, 'Protection OF', 46, 0),
(33, 'Casque', 5, 0),
(34, 'Colle', 7, 0),
(35, 'Fist OSKg', 1, 0),
(36, 'PBPO', 10, 0),
(37, 'Cle mixte chrome', 1, 0),
(38, 'Tournevis', 2, 0),
(39, 'Mousquetons', 1, 0),
(40, 'Bluelit', 3, 0),
(41, 'Polo', 1, 0),
(42, 'Chemise MKTN', 1, 0),
(43, 'Chemises', 3, 0),
(44, 'Capot Itor', 10, 0),
(45, 'Fosc CS2NT', 14, 0),
(46, 'Fosc A8', 20, 0),
(47, 'Disque dur 500 Go', 3, 0),
(48, 'Pantalon', 3, 0),
(49, 'Gilets', 3, 0),
(50, 'Bottes', 3, 0),
(51, 'Fist GC02', 5, 0),
(52, 'Teniso', 1, 0),
(53, 'PBO immeuble', 2, 0),
(54, 'Boite Fist', 2, 0),
(55, 'Boite Fosc', 5, 0);

-- --------------------------------------------------------

--
-- Structure de la table `pa`
--

CREATE TABLE `pa` (
  `idPA` int(20) NOT NULL,
  `numPA` varchar(20) NOT NULL,
  `idPM` int(11) NOT NULL,
  `capaentree` varchar(50) NOT NULL,
  `TRentree` int(20) NOT NULL,
  `NBcablesortie` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `pa`
--

INSERT INTO `pa` (`idPA`, `numPA`, `idPM`, `capaentree`, `TRentree`, `NBcablesortie`) VALUES
(4, '00025', 1, '144', 150071, 6);

-- --------------------------------------------------------

--
-- Structure de la table `pb`
--

CREATE TABLE `pb` (
  `idPB` int(20) NOT NULL,
  `idCable` int(11) NOT NULL,
  `TRpb` int(20) NOT NULL,
  `PT` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `pb`
--

INSERT INTO `pb` (`idPB`, `idCable`, `TRpb`, `PT`) VALUES
(43, 9, 0, '0'),
(44, 9, 0, '0'),
(45, 9, 0, '0'),
(46, 9, 0, '0'),
(47, 9, 0, '0'),
(48, 10, 0, '0'),
(49, 10, 0, '0'),
(50, 10, 0, '0'),
(51, 11, 0, '0'),
(52, 12, 0, '0'),
(53, 12, 0, '0'),
(54, 13, 1, '000123'),
(55, 13, 2, '000123'),
(56, 13, 3, '000124'),
(57, 13, 4, '000125'),
(58, 13, 5, '000126'),
(59, 14, 1, '000127'),
(60, 14, 2, '000127'),
(61, 14, 3, '000128'),
(62, 14, 4, '000129'),
(63, 14, 5, '000130'),
(64, 14, 6, '000131');

-- --------------------------------------------------------

--
-- Structure de la table `pm`
--

CREATE TABLE `pm` (
  `idPM` int(20) NOT NULL,
  `numPM` varchar(20) NOT NULL,
  `adresse` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `pm`
--

INSERT INTO `pm` (`idPM`, `numPM`, `adresse`) VALUES
(1, '00008', 'Henin Beaumont');

-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

CREATE TABLE `projet` (
  `idProjet` int(20) NOT NULL,
  `typeProjet` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `projet`
--

INSERT INTO `projet` (`idProjet`, `typeProjet`) VALUES
(27, 'D2'),
(28, 'D1'),
(29, 'FET NAT'),
(30, 'B2B'),
(31, 'VTL');

-- --------------------------------------------------------

--
-- Structure de la table `rapport`
--

CREATE TABLE `rapport` (
  `idRapport` int(11) NOT NULL,
  `objetRapport` varchar(54) NOT NULL,
  `dateRapport` date NOT NULL,
  `commentaireRapport` varchar(500) NOT NULL,
  `idTechnicien` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `rapport`
--

INSERT INTO `rapport` (`idRapport`, `objetRapport`, `dateRapport`, `commentaireRapport`, `idTechnicien`) VALUES
(1, 'Rapport RAS', '2018-06-26', 'Rien à signaler', 1);

-- --------------------------------------------------------

--
-- Structure de la table `stockage`
--

CREATE TABLE `stockage` (
  `idStockage` int(20) NOT NULL,
  `typeStockage` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `stockage`
--

INSERT INTO `stockage` (`idStockage`, `typeStockage`) VALUES
(12, 'BOXER'),
(13, 'C14'),
(14, 'MASTER'),
(15, 'CONTAINER'),
(16, 'MASTER C'),
(17, 'C8');

-- --------------------------------------------------------

--
-- Structure de la table `technicien`
--

CREATE TABLE `technicien` (
  `idTechnicien` int(11) NOT NULL,
  `nomTechnicien` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `technicien`
--

INSERT INTO `technicien` (`idTechnicien`, `nomTechnicien`) VALUES
(1, 'Halim');

-- --------------------------------------------------------

--
-- Structure de la table `touret`
--

CREATE TABLE `touret` (
  `idTouret` int(20) NOT NULL,
  `nomTouret` varchar(55) NOT NULL,
  `typeCable` varchar(55) NOT NULL,
  `reference` varchar(55) NOT NULL,
  `longueur` varchar(55) NOT NULL,
  `longueurTotale` int(20) NOT NULL,
  `Rendu` tinyint(1) NOT NULL,
  `dateRendu` date DEFAULT NULL,
  `idClient` int(20) NOT NULL,
  `idProjet` int(20) NOT NULL,
  `idStockage` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `touret`
--

INSERT INTO `touret` (`idTouret`, `nomTouret`, `typeCable`, `reference`, `longueur`, `longueurTotale`, `Rendu`, `dateRendu`, `idClient`, `idProjet`, `idStockage`) VALUES
(1, '24FO', '24', '', '0', 1200, 0, NULL, 10, 27, 12),
(2, '36 FO', '36', '', '109', 109, 1, '2018-05-22', 10, 27, 13),
(3, '6 FO', '6', '', '947', 947, 1, '2018-05-22', 10, 27, 14),
(4, '6 FO', '6', '', '1200', 1200, 0, '0000-00-00', 10, 27, 15),
(5, '12 FO', '12', '', '1200', 1200, 1, '0000-00-00', 10, 27, 12),
(6, '12 FO', '12', '', '939', 939, 0, '0000-00-00', 10, 27, 13),
(7, '24 FO BLANC', '24', '', '437', 437, 0, '0000-00-00', 10, 27, 13),
(8, '36 FO BLANC', '36', '', '0', 560, 0, '0000-00-00', 10, 27, 13),
(9, '12 FO BLANC', '12', '', '', 0, 1, '0000-00-00', 10, 27, 13),
(10, '36 FO', '12', '', '271', 271, 0, '0000-00-00', 10, 27, 13),
(11, '144 FO', '144', '', '-600', 0, 1, '0000-00-00', 10, 28, 16),
(12, '72 FO', '72', '', '', 0, 1, '0000-00-00', 11, 29, 17),
(13, '144 FO', '144', '', '', 0, 1, '0000-00-00', 11, 29, 17),
(14, '24 FO', '24', '', '', 0, 1, '0000-00-00', 11, 29, 13),
(15, '48 FO', '48', '', '', 0, 1, '0000-00-00', 11, 29, 13),
(16, '24 FO', '24', '12522791', '1290', 1290, 0, '0000-00-00', 11, 30, 17),
(17, '24 FO', '24', '156017', '1615', 1615, 0, '0000-00-00', 11, 30, 17),
(18, '12 FO', '12', '8XCH1586', '327', 327, 1, '0000-00-00', 11, 31, 17),
(19, 'test', '24', 'azeaz', '50', 2600, 0, '0000-00-00', 10, 27, 12);

-- --------------------------------------------------------

--
-- Structure de la table `vehicule`
--

CREATE TABLE `vehicule` (
  `idVehicule` int(30) NOT NULL,
  `immat` varchar(50) NOT NULL,
  `modele` varchar(50) NOT NULL,
  `kilomActuelle` varchar(50) NOT NULL,
  `idChauffeur` int(20) NOT NULL,
  `idEtat` int(50) NOT NULL,
  `dateReception` date NOT NULL,
  `dateRetour` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `vehicule`
--

INSERT INTO `vehicule` (`idVehicule`, `immat`, `modele`, `kilomActuelle`, `idChauffeur`, `idEtat`, `dateReception`, `dateRetour`) VALUES
(86, 'EN-626-FG', 'Renault Master', '25000', 14, 2, '2018-05-01', '2018-06-01'),
(90, 'ER-736-DK', 'Mercedes Vito', '65000', 12, 2, '2018-06-06', '2018-06-06'),
(95, 'BM-605-DJ', 'Nacelle', '77888', 16, 2, '2018-05-19', '2018-06-30'),
(96, 'EL-920-BJ', 'Fiat Ducato', '15000', 8, 2, '2018-06-05', '2018-06-28'),
(97, 'DX-518-VQ', 'Renault Trafic', '45000', 9, 2, '2018-06-01', '2018-06-27'),
(98, 'ER-065-LN', 'Ford Transit', '12500', 6, 2, '2018-06-07', '2018-06-22'),
(99, 'ER-766-DK', 'Mercedes Vito', '45000', 13, 2, '2018-06-14', '2018-06-22'),
(101, 'A Demander', 'Grand Master', '18654', 10, 2, '2018-06-23', '2018-06-30'),
(102, 'ER-080-VL', 'Ford', '67987', 7, 2, '2018-06-06', '2018-06-30');

-- --------------------------------------------------------

--
-- Structure de la table `vehiculeprobleme`
--

CREATE TABLE `vehiculeprobleme` (
  `idProbleme` int(11) NOT NULL,
  `dateDepot` date NOT NULL,
  `dateRecup` date NOT NULL,
  `idVehicule` int(11) NOT NULL,
  `idEtat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `vehiculeprobleme`
--

INSERT INTO `vehiculeprobleme` (`idProbleme`, `dateDepot`, `dateRecup`, `idVehicule`, `idEtat`) VALUES
(23, '2018-05-18', '2018-05-31', 86, 3);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `cablesortie`
--
ALTER TABLE `cablesortie`
  ADD PRIMARY KEY (`idCable`),
  ADD KEY `idPA` (`idPA`);

--
-- Index pour la table `chauffeur`
--
ALTER TABLE `chauffeur`
  ADD PRIMARY KEY (`idChauffeur`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`idClient`);

--
-- Index pour la table `connexion`
--
ALTER TABLE `connexion`
  ADD PRIMARY KEY (`id_connexion`),
  ADD KEY `id_droit` (`id_droit`);

--
-- Index pour la table `droits`
--
ALTER TABLE `droits`
  ADD PRIMARY KEY (`id_droit`);

--
-- Index pour la table `etat`
--
ALTER TABLE `etat`
  ADD PRIMARY KEY (`idEtat`);

--
-- Index pour la table `fibre`
--
ALTER TABLE `fibre`
  ADD PRIMARY KEY (`idFibre`),
  ADD KEY `idPM` (`idPM`);

--
-- Index pour la table `historique`
--
ALTER TABLE `historique`
  ADD PRIMARY KEY (`idHistorique`),
  ADD KEY `idTouret` (`idTouret`),
  ADD KEY `idClient` (`idClient`),
  ADD KEY `idProjet` (`idProjet`);

--
-- Index pour la table `historiquechauffeur`
--
ALTER TABLE `historiquechauffeur`
  ADD PRIMARY KEY (`idHistoriqueC`),
  ADD KEY `idVehicule` (`idVehicule`),
  ADD KEY `idChauffeur` (`idChauffeur`);

--
-- Index pour la table `historiquekilometrage`
--
ALTER TABLE `historiquekilometrage`
  ADD PRIMARY KEY (`idHistoriqueK`),
  ADD KEY `idVehicule` (`idVehicule`);

--
-- Index pour la table `materiel`
--
ALTER TABLE `materiel`
  ADD PRIMARY KEY (`idMateriel`);

--
-- Index pour la table `pa`
--
ALTER TABLE `pa`
  ADD PRIMARY KEY (`idPA`),
  ADD KEY `idPM` (`idPM`);

--
-- Index pour la table `pb`
--
ALTER TABLE `pb`
  ADD PRIMARY KEY (`idPB`),
  ADD KEY `idCable` (`idCable`);

--
-- Index pour la table `pm`
--
ALTER TABLE `pm`
  ADD PRIMARY KEY (`idPM`);

--
-- Index pour la table `projet`
--
ALTER TABLE `projet`
  ADD PRIMARY KEY (`idProjet`);

--
-- Index pour la table `rapport`
--
ALTER TABLE `rapport`
  ADD PRIMARY KEY (`idRapport`),
  ADD KEY `idTechnicien` (`idTechnicien`);

--
-- Index pour la table `stockage`
--
ALTER TABLE `stockage`
  ADD PRIMARY KEY (`idStockage`);

--
-- Index pour la table `technicien`
--
ALTER TABLE `technicien`
  ADD PRIMARY KEY (`idTechnicien`);

--
-- Index pour la table `touret`
--
ALTER TABLE `touret`
  ADD PRIMARY KEY (`idTouret`),
  ADD KEY `idClient` (`idClient`),
  ADD KEY `idProjet` (`idProjet`),
  ADD KEY `idStockage` (`idStockage`);

--
-- Index pour la table `vehicule`
--
ALTER TABLE `vehicule`
  ADD PRIMARY KEY (`idVehicule`),
  ADD KEY `idChauffeur` (`idChauffeur`),
  ADD KEY `idEtat` (`idEtat`);

--
-- Index pour la table `vehiculeprobleme`
--
ALTER TABLE `vehiculeprobleme`
  ADD PRIMARY KEY (`idProbleme`),
  ADD KEY `idVehicule` (`idVehicule`),
  ADD KEY `idEtat` (`idEtat`),
  ADD KEY `idVehicule_2` (`idVehicule`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `cablesortie`
--
ALTER TABLE `cablesortie`
  MODIFY `idCable` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `chauffeur`
--
ALTER TABLE `chauffeur`
  MODIFY `idChauffeur` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `idClient` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `connexion`
--
ALTER TABLE `connexion`
  MODIFY `id_connexion` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `etat`
--
ALTER TABLE `etat`
  MODIFY `idEtat` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `fibre`
--
ALTER TABLE `fibre`
  MODIFY `idFibre` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT pour la table `historique`
--
ALTER TABLE `historique`
  MODIFY `idHistorique` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `historiquechauffeur`
--
ALTER TABLE `historiquechauffeur`
  MODIFY `idHistoriqueC` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT pour la table `historiquekilometrage`
--
ALTER TABLE `historiquekilometrage`
  MODIFY `idHistoriqueK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT pour la table `materiel`
--
ALTER TABLE `materiel`
  MODIFY `idMateriel` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT pour la table `pa`
--
ALTER TABLE `pa`
  MODIFY `idPA` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `pb`
--
ALTER TABLE `pb`
  MODIFY `idPB` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT pour la table `pm`
--
ALTER TABLE `pm`
  MODIFY `idPM` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `projet`
--
ALTER TABLE `projet`
  MODIFY `idProjet` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT pour la table `rapport`
--
ALTER TABLE `rapport`
  MODIFY `idRapport` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `stockage`
--
ALTER TABLE `stockage`
  MODIFY `idStockage` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `technicien`
--
ALTER TABLE `technicien`
  MODIFY `idTechnicien` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `touret`
--
ALTER TABLE `touret`
  MODIFY `idTouret` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pour la table `vehicule`
--
ALTER TABLE `vehicule`
  MODIFY `idVehicule` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT pour la table `vehiculeprobleme`
--
ALTER TABLE `vehiculeprobleme`
  MODIFY `idProbleme` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `historiquekilometrage`
--
ALTER TABLE `historiquekilometrage`
  ADD CONSTRAINT `historiquekilometrage_ibfk_1` FOREIGN KEY (`idVehicule`) REFERENCES `vehicule` (`idVehicule`);

--
-- Contraintes pour la table `pa`
--
ALTER TABLE `pa`
  ADD CONSTRAINT `pa_ibfk_1` FOREIGN KEY (`idPM`) REFERENCES `pm` (`idPM`);

--
-- Contraintes pour la table `pb`
--
ALTER TABLE `pb`
  ADD CONSTRAINT `pb_ibfk_1` FOREIGN KEY (`idCable`) REFERENCES `cablesortie` (`idCable`);

--
-- Contraintes pour la table `rapport`
--
ALTER TABLE `rapport`
  ADD CONSTRAINT `rapport_ibfk_1` FOREIGN KEY (`idTechnicien`) REFERENCES `technicien` (`idTechnicien`);

--
-- Contraintes pour la table `touret`
--
ALTER TABLE `touret`
  ADD CONSTRAINT `touret_ibfk_1` FOREIGN KEY (`idClient`) REFERENCES `client` (`idClient`),
  ADD CONSTRAINT `touret_ibfk_2` FOREIGN KEY (`idProjet`) REFERENCES `projet` (`idProjet`),
  ADD CONSTRAINT `touret_ibfk_3` FOREIGN KEY (`idStockage`) REFERENCES `stockage` (`idStockage`);

--
-- Contraintes pour la table `vehiculeprobleme`
--
ALTER TABLE `vehiculeprobleme`
  ADD CONSTRAINT `vehiculeprobleme_ibfk_1` FOREIGN KEY (`idVehicule`) REFERENCES `vehicule` (`idVehicule`),
  ADD CONSTRAINT `vehiculeprobleme_ibfk_2` FOREIGN KEY (`idEtat`) REFERENCES `etat` (`idEtat`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
