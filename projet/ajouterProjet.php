<?php 
$bdd = new PDO('mysql:host=localhost;dbname=tableaumktn;charset=utf8', 'root', ''); 
session_start();

if ($_SESSION['id_connexion']==null) {
  header("Location: ../accueil/connexion.php");
}

ini_set( 'display_errors', 'on' );
error_reporting( E_ALL );

if (isset($_POST['boutonprojet'])) {

        //Insertion des données dans la bdd
          $typeProjet = htmlspecialchars(trim($_POST['typeProjet']));

$req3 = $bdd->prepare('INSERT INTO projet(typeProjet) VALUES(:typeProjet)');
        $req3->execute(array(
              'typeProjet' => $typeProjet
               ));

    } 
   ?>
   <html>
<head>
 <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link href="../www/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  <title>Formulaire</title>
</head>
<body class="index">
     <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>

<?php include ("../menu/menu.php") ?>

  <form action="#" method="POST">

    <fieldset>
      <table>
          <legend style = "color : #156094">Création d'un projet</legend>
<tr>
          <th><label for="typeProjet">Type de projet : </label></th>
          <td><input  name="typeProjet"/></td> 
</tr>
        <tr>
          <th></th>
         <td><br><button type="submit" name="boutonprojet" class="btn btn-primary"><b>Ajouter</b></button></td>
       </tr>

</table>
</fieldset>

<div class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>

</body>
    <script src="../www/bootstrap/js/jquery.js"></script>
    <script src="../www/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../www/js/bootstrap.min.js"></script>
</html>

