<?php 
session_start();
$bdd = new PDO('mysql:host=localhost;dbname=tableaumktn;charset=utf8', 'root', '');
if ($_SESSION['id_connexion']==null) {
  header("Location: ../accueil/connexion.php");
}
?>
<html>
  <head>
  <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
        <script src="../js/jQuery.msgBox-master/scripts/jquery.msgBox.js" type="text/javascript"></script>
    <link href="../js/jQuery.msgBox-master/styles/msgBoxLight.css" rel="stylesheet" type="text/css">
    <script type="text/javascript">
    function example3(id) {
            $.msgBox({
                title: "Supprimer",
                content: "Êtes vous sûr de vouloir supprimer ce stockage?",
                type: "confirm",
                buttons: [{ value: "Oui" }, { value: "Non" }, { value: "Annuler"}],
                success: function (result) {
                    if (result == "Oui") {
                        document.location.href="supprimerStockage.php?id="+id;
                        
                    }
                }
            });
        }
    </script>
  <title>Formulaire</title>
 </head>
<body class="index">
     <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>
  <?php include ("../menu/menu.php") 
  ?>
 <h2> Affichage des stockages</h2>
    <table class="table table-striped">
<thead>
  <tr class="stockage">
    <th>Numéro du stockage </th>
    <th>Type de stockage </th>
    <th></th>
  </tr>
</thead>
      <?php
      
         $req = $bdd->query("SELECT * FROM stockage ");
       $i=1;
      while ($stockage = $req->fetch()) { 
       
?>
  <tbody>
<tr>
  <td><?php echo $i; ?></td>
  <td><?php echo $stockage['typeStockage']; ?></td>
  <td><a href="#" onClick="example3(<?php echo $stockage['idStockage'] ?>)">Supprimer</a></td>
 </tr>
  </tbody>

<?php
$i++;
}
?>
</table>
<div class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>
</body>

</html>