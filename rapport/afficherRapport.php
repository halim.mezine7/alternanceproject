<?php  
session_start();
$connect = mysqli_connect("localhost", "root", "" , "tableaumktn");

  if ($_SESSION['id_connexion']==null) {
  header("Location: ../accueil/connexion.php");
}

$query= "SELECT * FROM rapport,technicien WHERE rapport.idTechnicien = technicien.idTechnicien";
$result = mysqli_query($connect,$query);


include('../PHPExcel/FichierExcel.php');
$fichier = new FichierExcel();

if(isset($_POST['export'])){
    $info = $_SESSION['donnees'];
    $fichier->Insertion($info);
    $fichier->output('Rapports');
  }

?>


<html>
<head>
  <meta charset="UTF-8">
        <link rel="stylesheet" href="../css/mktn.css"/>
        <link rel="stylesheet" href="../css/menu.css"/>
        <link rel="stylesheet" type="text/css" href="../js/datatables.css">
        <link rel="stylesheet" type="text/css" href="../js/DataTables-1.10.16/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
        <script type="text/javascript" charset="utf8" src="../js/datatables.js">
        </script>
        <script type="text/javascript" charset="utf8" src="../js/DataTables-1.10.16/js/dataTables.bootstrap.min.js">
        </script>
        <script type="text/javascript" charset="utf8" src="../js/DataTables-1.10.16/js/jquery.dataTables.min.js">
        </script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
   		<script src="../js/jQuery.msgBox-master/scripts/jquery.msgBox.js" type="text/javascript"></script>
    	<link href="../js/jQuery.msgBox-master/styles/msgBoxLight.css" rel="stylesheet" type="text/css">
   <title>Formulaire</title>
</head>

<body class="index">
     <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>
     <?php
 include ("../menu/menu.php"); 
 
 $donnees = "Objet du Rapport ; date du Rapport ; Commentaire Rapport ; Nom du Technicien"."\n";
 ?>

<center><h2>Rapports des techniciens</h2></center>
<div class="container">
<div class="container">
<table class="display" id="example"">

<thead>
  <tr class="rapport">
    <th>Objet du Rapport</th>
    <th>Date du Rapport</th>
    <th>Commentaire Rapport</th>
    <th>Nom du Technicien</th>
  </tr>
</thead>

<tbody>
  <?php

while ($rapport = mysqli_fetch_array($result)){ 
$idr = $rapport['idRapport'];
?>

  <tr class="gradeX">
  <td><?php echo $rapport['objetRapport']; $donnees = $donnees . $rapport['objetRapport'].";"; ?></td>
  <td><?php echo $rapport['dateRapport']; $donnees = $donnees . $rapport['dateRapport'].";"; ?></td>
  <td><?php echo $rapport['commentaireRapport']; $donnees = $donnees . $rapport['commentaireRapport'].";"; ?></td>
  <td><?php echo $rapport['nomTechnicien']; $donnees = $donnees . $rapport['nomTechnicien'].";" ; ?></td>
  </tr>

<?php
}
?>
</tbody>
</table>

<form action="" method="POST">
    <button type="submit" name="export" class="btn btn-primary"><b>Exporter excel</b></button>
</form>

<script type="text/javascript">
$(document).ready(function () {

    $('#example').DataTable({

        language: {

            url: "../js/French.json"

        }

    });

});
</script>

<div border ="solid" class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>

<?php $_SESSION['donnees'] = $donnees; ?>
</body>
</html>