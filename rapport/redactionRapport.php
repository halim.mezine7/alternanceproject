<?php 
$bdd = new PDO('mysql:host=localhost;dbname=tableaumktn;charset=utf8', 'root', ''); 
session_start();
ini_set( 'display_errors', 'on' );
error_reporting( E_ALL );

if(isset($_POST['boutonrapport'])){
	$objetRapport = htmlspecialchars(trim($_POST['objetRapport']));
	$dateRapport = htmlspecialchars(trim($_POST['dateRapport']));
	$commentaireRapport = htmlspecialchars(trim($_POST['commentaireRapport']));


	$reqRapport = $bdd->prepare('INSERT INTO rapport(objetRapport, dateRapport,commentaireRapport) VALUES(:objetRapport, :dateRapport, :commentaireRapport)');
          $reqRapport->execute(array(
              'objetRapport' => $objetRapport,
              'dateRapport' => $dateRapport,
              'commentaireRapport' => $commentaireRapport
               ));

}

?>

<html>
<head>
  <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link href="../www/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  <title>Formulaire</title>
</head>
<body class="index">
     <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>

<?php include ("../menu/menu.php");

?>

  <form action="#" method="POST">

    <fieldset>
      <table>
    <legend style="color : #156094">Rédaction du rapport Journalier</legend>
        <tr>
          <th><br><label for="objetRapport">Objet du Rapport : </label></th>
          <td><br><input type="text" name="objetRapport" style="width: 200px;" /></td>  
        </tr>

        <tr>
        	<th><br><label for="dateRapport">Date du Rapport : </label></th>
        	<td><br><br><input type="date" name="dateRapport" style="width: 200px;" /></td>
        </tr>

        <tr>
        	<th><br><label for="commentaireRapport">Commentaire du Rapport : </label></th>
        	<td><br><br><textarea name="commentaireRapport" rows="10" cols="50" /></textarea></td>
        </tr>

        <tr>
          <th></th>
         <td><br><button type="submit" name="boutonrapport" class="btn btn-primary"><b>Ajouter le rapport</b></button></td>
       </tr>

    </table>
</fieldset>

<div border="solid" class="footer">
  <br>
  TEAM CR ©
    <br><br>
</div>

</body>
    <script src="/www/bootstrap/js/jquery.js"></script>
    <script src="/www/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="/www/js/bootstrap.min.js"></script>
</html>
