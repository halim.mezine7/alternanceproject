<?php
session_start();
$bdd = new PDO('mysql:host=localhost;dbname=tableaumktn;charset=utf8', 'root', ''); 

ini_set( 'display_errors', 'on' );
error_reporting( E_ALL );

if (isset($_POST['connexion'])) {
  $login = htmlspecialchars(trim($_POST['login']));
  $mdp = htmlspecialchars(trim($_POST['mdp']));

      if($login&&$mdp) //Si touts les champs sont entrés
           {
            $requser = $bdd->prepare("SELECT * FROM connexion WHERE login = ? AND mdp = ? ");
            $requser -> execute(array($login, $mdp));
            $userexist = $requser -> rowCount();
            $userinfo = $requser -> fetch();
              if($userexist ==1)
              {
                $_SESSION['id_connexion'] = $userinfo ['id_connexion'];
                $_SESSION['login'] = $userinfo ['login'];
                $_SESSION['mdp'] = $userinfo ['mdp'];
                $_SESSION['id_droit'] = $userinfo ['id_droit'];

                header("Location: index.php");

              } else $messageErreur = '<p class = "phrase">Nom d\'utilisateur ou mot de passe introuvable <p/>';

      } else $messageErreur = '<p class = "phrase"> Veuillez saisir tous les champs.<p/>';
}
?>


<html>
<head>
  <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link rel="stylesheet" type="text/css" href="../js/datatables.css">
        <link rel="stylesheet" type="text/css" href="../js/DataTables-1.10.16/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
        <script type="text/javascript" charset="utf8" src="../js/datatables.js">
        </script>
        <script type="text/javascript" charset="utf8" src="../js/DataTables-1.10.16/js/dataTables.bootstrap.min.js">
        </script>
        <script type="text/javascript" charset="utf8" src="../js/DataTables-1.10.16/js/jquery.dataTables.min.js">
        </script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>

<body class="connexion">

  <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>


<div class="imgconnexion">
<div class="formconnexion">

<center><h1>Connexion au site de MKTN Group</h1></center>
<form class="form-horizontal" action="#" method="post">
  <div class="container">

    <div class="form-group">
        <label for="username">Votre identifiant</label>
        <input type="text" class="form-control" id="username" name="login" />
    </div>

    <div class="form-group">
        <label for="password">Mot de passe</label>
        <input type="password" class="form-control" id="password" name="mdp" />
    </div>
    
        <button class="btn btn-block btn-primary btn-lg" name="connexion"><b>Connexion</b></button>
        </div>
</form>

<div border="solid" class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>
</body>

</html>