<?php 
$bdd = new PDO('mysql:host=localhost;dbname=tableaumktn;charset=utf8', 'root', ''); 
session_start();

ini_set( 'display_errors', 'on' );
error_reporting( E_ALL );
if ($_SESSION['id_connexion']==null) {
  header("Location: ../accueil/connexion.php");
}

$req = 'SELECT * FROM pm';
    $pm = $bdd->prepare($req);
    $pm->execute();
    $donneesPM = $pm->fetchAll(PDO::FETCH_ASSOC);

if (isset($_POST['bouton'])) {
      $idPM = $_POST['idPM'];
      $_SESSION['idPM'] = $idPM;
      header('Location: remplirTableau2.php');
      $_SESSION["cassette"] = "A";
      $_SESSION["tete"] = 0;

    }

?>
<html>
<head>
  <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link href="../www/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  <title>Formulaire</title>
</head>
<body class="index">
     <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>
     <?php
 include ("../menu/menu.php") ?>
  <form action="#" method="POST">
    <fieldset>
      <table>
          <legend style = "color : #156094">Remplissage du PM</legend>

          <div class="form-group"> 
        <tr>   
          <th><label for="idPM">PM : </label></th>
          <td><SELECT value="idPM" name="idPM" type="text" size="1" style="width: 200px;">
                  
                  <?php
                  foreach ($donneesPM AS $donneePM)
                  {
                    ?>
                  
                  <option value="<?php echo $donneePM['idPM']; ?>"><?php echo $donneePM['numPM']." - ".$donneePM['adresse']; ?></option>

                  <?php
                  }
                  ?>
              </SELECT></td>
        </tr></div>

        <tr>
              <th></th>
              <td><br><button type="submit" name="bouton"  class="btn btn-primary"><b>Choisir</b></button></td>
        </tr>
      </table>
      </fieldset>
  </form>

<div class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>
</body>
    <script src="../www/bootstrap/js/jquery.js"></script>
    <script src="../www/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../www/js/bootstrap.min.js"></script>
</html>