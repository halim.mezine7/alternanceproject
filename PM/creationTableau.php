<?php 
$bdd = new PDO('mysql:host=localhost;dbname=tableaumktn;charset=utf8', 'root', ''); 
session_start();
ini_set( 'display_errors', 'on' );
error_reporting( E_ALL );

if ($_SESSION['id_connexion']==null) {
  header("Location: ../accueil/connexion.php");
}
if (isset($_POST['bouton'])) {

        //Insertion des données dans la bdd
        $numPM = htmlspecialchars(trim($_POST['numPM']));
        $adresse = htmlspecialchars(trim($_POST['adresse']));

        $req = $bdd->prepare('INSERT INTO pm(numPM, adresse) VALUES(:numPM, :adresse)');
        
        $req->execute(array(
              'numPM' => $numPM,
              'adresse' => $adresse
               ));

    } 

?>

<?php
if (isset($_POST['bouton'])) {
      $numPM = $_POST['numPM'];
      $_SESSION['numPM'] = $numPM;
      $adresse = $_POST['adresse'];
      $_SESSION['adresse'] = $adresse;
      header('Location: remplirTableau.php');
     
    }
?>
<html>
<head>
  <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link href="../www/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  <title>Formulaire</title>
</head>
<body class="index">
     <div class="banniere">
        <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>
     <?php
 include ("../menu/menu.php") ?>
  <form action="#" method="POST">
    <fieldset>
          <legend style = "color : #156094">Création du PM</legend>
    <table>
        <tr>
          <th><label for="numPM">Numéro PM: </label></th>
          <td><input  name="numPM"/></td>
        </tr> 

        <tr>
          <th><br><label for="adresse">Adresse PM: </label></th>
          <td><br><input type="text" name="adresse"/></td>
        <tr>

        <tr>
          <th></th>
          <td><br><button type="submit" name="bouton" class="btn btn-primary"><b>Ajouter</b></button></td>
        </tr>

      </fieldset>
  </form>

  <table>
    
  </table>
    <?php
      if (isset($message)) {
        echo $message;
      }

    ?>
<div class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>
</body>
    <script src="../www/bootstrap/js/jquery.js"></script>
    <script src="../www/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../www/js/bootstrap.min.js"></script>
</html>