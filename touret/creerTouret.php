<?php 
$bdd = new PDO('mysql:host=localhost;dbname=tableaumktn;charset=utf8', 'root', ''); 
session_start();
if ($_SESSION['id_connexion']==null) {
  header("Location: ../accueil/connexion.php");
}

ini_set( 'display_errors', 'on' );
error_reporting( E_ALL );

$req = 'SELECT * FROM client';
    $client = $bdd->prepare($req);
    $client->execute();
    $donneesClient = $client->fetchAll(PDO::FETCH_ASSOC);

$req1 = 'SELECT * FROM projet';
    $projet = $bdd->prepare($req1);
    $projet->execute();
    $donneesProjet = $projet->fetchAll(PDO::FETCH_ASSOC);

$req2 = 'SELECT * FROM stockage';
    $stockage = $bdd->prepare($req2);
    $stockage->execute();
    $donneesStockage = $stockage->fetchAll(PDO::FETCH_ASSOC);
    

if (isset($_POST['boutouret'])) {

        //Insertion des données dans la bdd
          $nomTouret = htmlspecialchars(trim($_POST['nomTouret'])); 
         // $_SESSION['nomTouret'] = $nomTouret; //SESSION = Sauvegarde les données ( par exemple les données d'un profil sont sauvegardés) et POST pour recuperer les données d'un formulaire HTML
          $typeCable = htmlspecialchars(trim($_POST['typeCable']));
         $_SESSION['typeCable'] = $typeCable;
          $reference = htmlspecialchars(trim($_POST['reference']));
          $idClient = htmlspecialchars(trim($_POST['idClient']));
          $idProjet = htmlspecialchars(trim($_POST['idProjet']));
          $idStockage = htmlspecialchars(trim($_POST['idStockage']));
          $longueur = htmlspecialchars(trim($_POST['longueur']));

       // $requete = "INSERT INTO touret(nomTouret, typeCable, typeProjet, reference, stockage, longueur) VALUES(:nomTouret, :typeCable, :reference, :typeProjet, :stockage, :longueur)";
        $req3 = $bdd->prepare('INSERT INTO touret(nomTouret, typeCable, reference, longueur, longueurTotale, idClient, idProjet, idStockage) VALUES(:nomTouret, :typeCable, :reference, :longueur, :longueurTotale, :idClient, :idProjet, :idStockage)');
        $req3->execute(array(
              'nomTouret' => $nomTouret,
              'typeCable' => $typeCable,
              'reference' => $reference,
              'longueur' => $longueur,
              'longueurTotale'=>$longueur,
              'idClient' => $idClient,
              'idProjet' => $idProjet,
              'idStockage' => $idStockage
               ));

    } 
   
     
// echo$requete;
?>

<html>
<head>
  <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link href="../www/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  <title>Formulaire</title>
</head>
<body class="index">
     <div class="banniere">
         <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>

<?php include ("../menu/menu.php") ?>
  <form action="#" method="POST">

    <fieldset>
      <table>
          <legend style = "color : #156094">Création du Touret</legend>
        <tr>
          <th><label for="nomTouret">Nom du Touret : </label></th>
          <td><input  name="nomTouret" style="width: 200px;" /><td>
        </tr>
        <tr>
          <th><br><label for="typeCable">Capacité cable : </label></th>

          <td><br><SELECT value="typeCable" name="typeCable" type="text" size="1" style="width: 200px;">
          <OPTION <?php if (isset($_POST['boutouret']) AND $_SESSION["typeCable"]==6) { 
            echo "selected"; } ?>>6</OPTION>
          <OPTION <?php if (isset($_POST['boutouret']) AND $_SESSION["typeCable"]==12) { 
            echo "selected"; } ?>>12</OPTION>
          <OPTION <?php if (isset($_POST['boutouret']) AND $_SESSION["typeCable"]==24) { 
            echo "selected"; } ?>>24</OPTION>
          <OPTION <?php if (isset($_POST['boutouret']) AND $_SESSION["typeCable"]==36) { 
            echo "selected"; } ?>>36</OPTION>
          <OPTION <?php if (isset($_POST['boutouret']) AND $_SESSION["typeCable"]==48) { 
            echo "selected"; } ?>>48</OPTION>
          <OPTION <?php if (isset($_POST['boutouret']) AND $_SESSION["typeCable"]==72) { 
            echo "selected"; } ?>>72</OPTION>
          <OPTION <?php if (isset($_POST['boutouret']) AND $_SESSION["typeCable"]==96) { 
            echo "selected"; } ?>>96</OPTION>
          <OPTION <?php if (isset($_POST['boutouret']) AND $_SESSION["typeCable"]==144) { 
            echo "selected"; } ?>>144</OPTION>
         </SELECT></td>
       </tr>
         
         <tr>
          <th><br><label for="reference">Reference : </label></th>
          <td><br><input type="text" name="reference" style="width: 200px;" /></td>
         </tr>

         <tr>
          <th><br><label for="client">Nom du Client : </label></th>
          <td><br><SELECT name="idClient" value ="idClient" size="1" style="width: 200px;">
            <?php
                  foreach ($donneesClient AS $donneeClient)
                  {
                    ?>
                  
                  <option value="<?php echo $donneeClient['idClient'];?>">
                  <?php echo $donneeClient['nomClient']; ?> </option>
                  <?php
                  }
                  ?>
          </SELECT></td>
         </tr>

         <tr>
          <th><br><label for="idProjet">Projet : </label></th>
          <td><br><SELECT name="idProjet" value ="idProjet" size="1" style="width: 200px;">
            <?php
                  foreach ($donneesProjet AS $donneeProjet)
                  {
                    ?>
                  
                  <option value="<?php echo $donneeProjet['idProjet'];?>">
                  <?php echo $donneeProjet['typeProjet']; ?> </option>
                  <?php
                  }
                  ?>
         
          </SELECT></td>
         </tr>

         <tr>
          <th><br><label for="idStockage">Stockage : </label></th>
          <td><br><SELECT name="idStockage" value ="idStockage" size="1" style="width: 200px;">
            <?php
                  foreach ($donneesStockage AS $donneeStockage)
                  {
                    ?>
                  
                  <option value="<?php echo $donneeStockage['idStockage'];?>">
                  <?php echo $donneeStockage['typeStockage']; ?> </option>
                  <?php
                  }
                  ?>
         

                 </SELECT></td>
           </tr>

           <tr>
            <th><br><label for="longueur">Longueur Totale : </label></th>
            <td><br><input type="text" name="longueur" style="width: 200px;" /></td>
           </tr>
           
           <tr>
            <th></th>
            <td><br><button type="submit" name="boutouret" class="btn btn-primary"><b>Ajouter</b></button></td>
           </tr>
         
        </table>
      </fieldset>
  </form>

<?php

 if (isset($message)) {
        echo $message;
      }

    ?>
<div class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>
</body>
    <script src="../www/bootstrap/js/jquery.js"></script>
    <script src="../www/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../www/js/bootstrap.min.js"></script>
</html>
