<?php 
$bdd = new PDO('mysql:host=localhost;dbname=tableaumktn;charset=utf8', 'root', ''); 
session_start();
if ($_SESSION['id_connexion']==null) {
  header("Location: ../accueil/connexion.php");
}

ini_set( 'display_errors', 'on' );
error_reporting( E_ALL );

$req = 'SELECT * FROM touret';
    $touret = $bdd->prepare($req);
    $touret->execute();
    $donneesTouret = $touret->fetchAll(PDO::FETCH_ASSOC);

$req = 'SELECT * FROM projet';
    $projet = $bdd->prepare($req);
    $projet->execute();
    $donneesProjet = $projet->fetchAll(PDO::FETCH_ASSOC);

$req = 'SELECT * FROM client';
    $client = $bdd->prepare($req);
    $client->execute();
    $donneesClient = $client->fetchAll(PDO::FETCH_ASSOC);

if (isset($_POST['boutouret'])) {

        //Insertion des données dans la bdd
          $idTouret = htmlspecialchars(trim($_POST['idTouret']));
          $longueurPose = htmlspecialchars(trim($_POST['longueurPose']));
          $devis = htmlspecialchars(trim($_POST['devis']));
          $dateJour = htmlspecialchars(trim($_POST['dateJour']));
          $code = htmlspecialchars(trim($_POST['code']));
          $idClient = htmlspecialchars(trim($_POST['idClient']));
          $idProjet = htmlspecialchars(trim($_POST['idProjet']));


          //header('Location: historiqueTouret.php');

        $req = $bdd->prepare('INSERT INTO historique(longueurPose, devis, dateJour, code, idClient, idProjet, idTouret) VALUES(:longueurPose, :devis, :dateJour, :code, :idClient, :idProjet, :idTouret)');
        
        $req->execute(array(
              ':longueurPose' => $longueurPose,
              ':devis' => $devis,
              ':dateJour' => $dateJour,
              ':code' => $code,
              ':idClient' => $idClient,
              ':idProjet' => $idProjet,
              ':idTouret' => $idTouret
               ));

        $sel = $bdd->prepare("SELECT longueur, nomTouret FROM touret WHERE idTouret = ".$idTouret."");
        $sel->execute();
        $donneesLong = $sel->fetch(PDO::FETCH_ASSOC);

        $longueur = $donneesLong['longueur'];

        $LongueurRestante = $longueur - $longueurPose;

        $reqModif = ("UPDATE touret SET longueur = ".$LongueurRestante." WHERE idTouret = ".$idTouret."");
        $bdd ->exec($reqModif);

        if ($LongueurRestante <= 100) {
          $destinataire = 'antoinebandarra@gmail.com';
          $sujet = 'Gestion des touret';
          $message = "Attention, la longueur du touret ".$donneesLong['nomTouret']." est de ".$LongueurRestante."m";
          $entete  = 'From: MKTN'."\r\n".
                  'Reply-To: MKTN'."\r\n".
                  'X-Mailer: PHP/' . phpversion();
          mail($destinataire, $sujet, $message, $entete);
        }

    } 

?>

<html>
<head>
    <meta charset="utf-8" />
        <link rel="stylesheet" href="../css/mktn.css" />
        <link rel="stylesheet" href="../css/menu.css"/>
        <link href="../www/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../bootstrap-3.3.7-dist/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="../bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  <title>Formulaire</title>
</head>
<body class="index">
     <div class="banniere">
        <center><a href="../accueil/index.php"><img src="../images/logo.jpg" class ="arrondi"></a></center>
     </div>

<?php include ("../menu/menu.php") ?>
  <form action="#" method="POST">

    <fieldset>
      <table>
          <legend style = "color : #156094">Remplissage du Touret</legend>
        <tr>
          <th><br><label for="Touret">Choisir touret : </label></th>
          <td><br><SELECT name="idTouret" value ="idTouret" size="1" style="width: 150px; ">
            <?php
                  foreach ($donneesTouret AS $donneeTouret)
                  {
                    ?>
                  
                  <option value="<?php echo $donneeTouret['idTouret'];?>">
                  <?php echo $donneeTouret['nomTouret']; ?> </option>
                  <?php
                  }
                  ?>
          </SELECT></td>
        </tr>

        <tr>
          <th><br><label for="longueurPose" >Longueur posé : </label></th>
          <td><br><input  name="longueurPose" style="width: 150px; "/></td>
        </tr>

        <tr>
          <th><br><label for="Projet">Projet : </label></th>
          <td><br><SELECT name="idProjet" value ="idProjet" size="1" style="width: 150px; ">
            <?php
                  foreach ($donneesProjet AS $donneeProjet)
                  {
                    ?>
                  
                  <option value="<?php echo $donneeProjet['idProjet'];?>">
                  <?php echo $donneeProjet['typeProjet']; ?> </option>
                  <?php
                  }
                  ?>
          </SELECT></td>
        </tr>

        <tr>
          <th><br><label for="Client">Client : </label></th>
          <td><br><SELECT name="idClient" value ="idClient" size="1" style="width: 150px; ">
            <?php
                  foreach ($donneesClient AS $donneeClient)
                  {
                    ?>
                  
                  <option value="<?php echo $donneeClient['idClient'];?>">
                  <?php echo $donneeClient['nomClient']; ?> </option>
                  <?php
                  }
                  ?>
          </SELECT></td>
        </tr>

     
        <tr>
          <th><br><label for="devis">N°Devis : </label></th>
          <td><br><input name="devis" style="width: 150px; "/></td>
        </tr>

        <tr>
          <th><br><label for="dateJour">Date : </label></th>
          <td><br><input type ="date" name="dateJour" style="width: 150px; "/></td>
        </tr>

        <tr>
          <th><br><label for="code">Code : </label></th>
          <td><br><input type="text" name="code" style="width: 150px; "/></td>
        </tr>
       
        <tr>
          <th></th>
          <td><br><button type="submit" name="boutouret" class="btn btn-primary"><b>Ajouter</b></button></td>
        </tr>
      </table>   
      </fieldset>
  </form>

<?php

 if (isset($message)) {
        echo $message;
      }

    ?>
<div class="footer">
  <br>
  MKTN GROUP-2018 ©
    <br><br>
</div>
</body>
    <script src="../www/bootstrap/js/jquery.js"></script>
    <script src="../www/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../www/js/bootstrap.min.js"></script>
</html>
